<?php
/*!
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */

/**
 * BOOTSTRAP
 *
 * @version 1.0.0
 */

date_default_timezone_set('America/Sao_Paulo');
ini_set('display_errors', 'on');
error_reporting(E_ALL);

/**
 * Class Bootstrap
 *
 * @package \
 */
final class Bootstrap
{

    /**
     * @param array $args
     * @return string
     */
    private static function implodeArgs(array $args = [])
    {
        $total = count($args);
        $path = "";

        if ($total > 0) {
            if (strpos($args[0], "/") !== 0) {
                $args[0] = DIRECTORY_SEPARATOR . $args[0];
            }
        }
        if ($total > 1) {
            $path = implode(DIRECTORY_SEPARATOR, $args);
        } else if ($total == 1) {
            $path = $args[0];
        }

        $path = str_replace(
            DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR,
            DIRECTORY_SEPARATOR,
            $path
        );

        return $path;
    }

    /**
     * @return string
     */
    public static function getPathRoot()
    {
        return dirname(dirname(__FILE__)) . self::implodeArgs(func_get_args());
    }

    /**
     * @return string
     */
    public static function getBaseProject()
    {
        $baseRoot = self::getPathRoot();
        $baseProject = explode(DIRECTORY_SEPARATOR, $baseRoot);
        $baseProject = array_pop($baseProject);
        return $baseProject;
    }

    /**
     * @return string
     */
    public static function getPathPublic()
    {
        return self::getPathRoot(
            'public',
            self::implodeArgs(func_get_args())
        );
    }

    /**
     * @return string
     */
    public static function getPathSrc()
    {
        return self::getPathRoot(
            'src',
            self::implodeArgs(func_get_args())
        );
    }

    /**
     * @return string
     */
    public static function getPathApp()
    {
        return self::getPathRoot(
            "app",
            self::implodeArgs(func_get_args())
        );
    }

    /**
     * @return string
     */
    public static function getPathVendor()
    {
        return self::getPathRoot(
            "vendor",
            self::implodeArgs(func_get_args())
        );
    }

    /**
     * @param string $child
     * @return null|string
     */
    public static function getUrlProject($child = "")
    {
        if (isset($_SERVER['SERVER_NAME'])) {
            return "http://{$_SERVER['SERVER_NAME']}" .
            (strpos($_SERVER['SERVER_NAME'], 'localhost') !== false ?
                self::getBaseProject("public") : '') . $child;
        }
        return null;
    }

}

require_once Bootstrap::getPathVendor() . '/autoload.php';