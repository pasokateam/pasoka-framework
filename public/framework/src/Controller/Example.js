/**
 * Controller Example
 *
 * How to use:
 *
 * HTML CODE
 *
 * <psk-view controller="Example">
 *      <script data-main="framework/pasoka" src="/framework/kernel/require.js"></script>
 *      <psk-param name="cache" value="20141127" type="string">
 *      <psk-cookie name="product" value="10" expire="10" />
 * </psk-view>
 *
 *
 * @author Guilherme
 * @version 1.0.0
 */
(function (window, document, PASOKA) {

    "use strict";

    /**
     * @includes
     */
    PASOKA.include('PASOKA.Cookie');

    /**
     * @properties
     */
    PASOKA.properties({
        count: 7
    });

    /**
     * @sandbox
     * @description A
     */
    PASOKA.$(function (properties) {
        properties.count++;
    });

    /**
     * @sandbox
     * @description B
     */
    PASOKA.$(function (properties) {
        PASOKA.Cookie.set("count", properties);

        alert(PASOKA.CONFIG.get("count"))
    });
})(window, document, window.PASOKA || {});