/*!
 * MODULE Format 2014-01-08
 *  ____
 * |    | PASOKA
 * |    | FRAMEWORK
 * |____| [JS]
 *
 * Dependencies:
 * - Core pasoka.js
 *
 * Author:
 * - Guilherme Santos <gsantos@climatempo.com.br, guilhermedossantos91@gmail.com>
 *
 *  MIT license, see the license in the project root
 */
(function (window, document, PASOKA) {

    "use strict";

    /**
     * @public
     * @namespace PASOKA.Format
     * @desc Modulo para formatacao de dados
     * @version 1.0.0
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     */
    PASOKA.extend('Format', function Format() {

        /**
         * @public
         * @desc metodo autoexecutavel ao carregar modulo
         * @method main
         */
        this.main = function main() {};

    });
}(this, document, window.PASOKA = window.PASOKA || {}));