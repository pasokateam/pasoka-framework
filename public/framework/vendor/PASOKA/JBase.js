/*!
 * MODULE JBase 2014-02-07
 *  ____
 * |    | PASOKA
 * |    | FRAMEWORK
 * |____| [JS]
 *
 * Dependencies:
 * - Core pasoka.js
 *
 * Author:
 * - Guilherme Santos <gsantos@climatempo.com.br, guilhermedossantos91@gmail.com>
 *
 *  MIT license, see the license in the project root
 */
(function (window, document, PASOKA) {

    "use strict";

    /**
     * @public
     * @namespace PASOKA.JBase
     * @desc Modulo para consultas inteligentes em JSON
     * @version 1.0.0
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     */
    PASOKA.extend('JBase', function JBase() {

        /**
         * @private
         * @desc caminho do jbase
         * @type {string}
         */
        var jBaseDir;

        /**
         * @public
         * @desc metodo autoexecutavel ao carregar modulo
         * @method main
         */
        this.main = function main() {
            jBaseDir = PASOKA.getConfigCore('path').jbase;
        };

        /**
         * @public
         * @desc verifica se o objeto e um JQL valido
         * @method checkJQL
         * @param {Object} jql
         * @returns {boolean}
         */
        this.checkJQL = function checkJQL(jql) {

            var error = 0;

            if ("object" === typeof jql) {
                if ("string" !== typeof jql.base) {
                    error += 1;
                }
                if ("undefined" !== typeof jql.orderBy &&
                    "string" !== typeof jql.orderBy) {
                   error += 1;
                }
            } else {
                error += 1;
            }
            return error === 0;
        };

        /**
         * @public
         * @desc seleciona informacoes de um JSON externo
         * @method selectExternal
         * @param {Object} JQL
         * @param {Function} callback
         */
        this.selectExternal = function selectExternal(JQL, callback) {};

        /**
         * @public
         * @desc seleciona informacoes de um JSON
         * @method select
         * @param {Object} JQL
         * @param {Function} callback
         */
        this.select = function select(JQL, callback) {

            var
                rows = [],
                find = [],
                row,
                i = 0,
                limit,
                total,
                order,
                orderMultiple;

            order = function order(property) {
                var sortOrder = 1;
                property = property.split(' ASC').join('');
                if(property.indexOf(' DESC') !== -1) {
                    sortOrder = -1;
                    property = property.split(' DESC').join('');
                }
                return function (a,b) {
                    var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                    return result * sortOrder;
                };
            };

            orderMultiple = function orderMultiple() {
                var props = arguments;
                return function (obj1, obj2) {
                    var i = 0, result = 0, numberOfProperties = props.length;
                    while(result === 0 && i < numberOfProperties) {
                        result = order(props[i])(obj1, obj2);
                        i++;
                    }
                    return result;
                };
            };

            // verifica se o select pode ser feito
            if (this.checkJQL(JQL) && "function" === typeof callback) {

                // obtem json da base
                require(['json!' + jBaseDir + JQL.base + '.json'], function (json) {

                    // cria lista para consulta
                    if (!PASOKA.isArray(json)) {
                        rows.push(json);
                    } else {
                        rows = json;
                    }

                    total = rows.length;

                    if (total > 0) {

                        limit = "number" !== typeof JQL.limit ? total : JQL.limit;
                        if ("undefined" !== typeof JQL.orderBy) {
                            rows.sort(orderMultiple(JQL.orderBy));
                        }

                        if("function" !== typeof JQL.where) {
                            JQL.where = function() {return true;}
                        }

                        for (; i < total; i += 1) {
                            row = rows[i];
                            if (JQL.where(row) && find.length < limit) {
                                find.push(row);
                            }
                        }
                        rows = find;
                    }
                    callback(rows, rows.length);
                });
            }
        };
    });
}(this, document, window.PASOKA = window.PASOKA || {}));