/*!
 * MODULE Slider 2014-03-24
 *  ____
 * |    | PASOKA
 * |    | FRAMEWORK
 * |____| [JS]
 *
 * Dependencies:
 * - Core pasoka.js
 * - jQuery
 *
 * Author:
 * - Guilherme Santos <gsantos@climatempo.com.br, guilhermedossantos91@gmail.com>
 *
 *  MIT license, see the license in the project root
 */
(function (window, document, PASOKA, $) {

    "use strict";

    /**
     * @public
     * @namespace PASOKA.Slider
     * @version 1.0.0
     */
    PASOKA.extend("Slider", function Slider() {


        /**
         * @public
         * @description Slider status
         * @type {{PLAY: number, PAUSE: number, STOP: number}}
         */
        this.status = {
            PLAY: 0,
            PAUSE: 1,
            STOP: 2
        };


        /**
         * @public
         * @param {{fist:jQuery, previous:jQuery, pause:jQuery, play:jQuery, next:jQuery, last:jQuery, loop: jQuery
         * speedUp: jQuery, speedDown:jQuery}} buttonsParam
         * @constructor
         */
        this.Player = function Player(buttonsParam) {

            var buttons;

            if ("undefined" === typeof this) {
                return new PASOKA.Slider.Player(buttonsParam);
            }

            buttons = buttonsParam || {};

            /**
             * @public
             * @type {{FIRST: string, PREVIOUS: string, PAUSE: string, PLAY: string, NEXT: string, LAST: string, LOOP: string, SPEED_UP: string, SPEED_DOWN: string}}
             */
            this.button = {
                FIRST: "first",
                PREVIOUS: "previous",
                PAUSE: "pause",
                PLAY: "play",
                NEXT: "next",
                LAST: "last",
                LOOP: "loop",
                SPEED_UP: "speedUp",
                SPEED_DOWN: "speedDown"
            };


            /**
             * @public
             * @param {function} callback
             */
            this.each = function each(callback) {

                var button;

                if (PASOKA.typeValidate(callback).isFunction()) {
                    for (button in buttons) {
                        if (buttons.hasOwnProperty(button)) {
                            callback(buttons[button]);
                        }
                    }
                }
            };

            /**
             * @public
             * @param {string} button
             * @param {function} callback
             * @return {HTMLElement|jQuery|$}
             */
            this.click = function click(button, callback) {

                var elm;

                if (buttons.hasOwnProperty(button)) {

                    elm = buttons[button];

                    if (elm instanceof $ && PASOKA.typeValidate(callback).isFunction()) {
                        elm.off("click").on("click", function () {
                            $(this).parent().children().removeClass("active");
                            $(this).addClass("active");
                            callback();
                        });
                    }
                }

                return elm;
            };

        };


        /**
         * @public
         * @description basic slider
         * @param {object} configParam
         * @constructor
         */
        this.Simple = function Simple(configParam) {

            var

                /**
                 * @private
                 * @type {number}
                 */
                hash = new Date().getTime(),


                /**
                 * @private
                 * @type {string}
                 */
                dataImageAttr = "data-position-" + hash,

                /**
                 * @private
                 * @type {Slider}
                 */
                instance = this,

                /**
                 * @private
                 * @type {number}
                 */
                defaultVelocity = 900,

                /**
                 * @private
                 * @type {{}}
                 */
                config = {
                    autoPlay: false,
                    images: [],
                    loop: false,
                    velocity: defaultVelocity
                },

                /**
                 * @private
                 * @type {*|jQuery|HTMLElement}
                 */
                container,

                /**
                 * @private
                 * @type {number}
                 */
                position = 0,

                /**
                 * @private
                 * @type {jQuery|*}
                 */
                slides,

                /**
                 * @private
                 * @description
                 * @type {number}
                 */
                status = PASOKA.Slider.status.STOP,

                /**
                 * @private
                 * @type {number}
                 */
                intervalSlide,

                /**
                 * @private
                 * @description pre load images
                 * @param {Array|images|HTMLCollection} images
                 * @param {function} callback
                 */
                preLoadImages = function preLoadImages(images, callback) {

                    var imgObj, countLoad = 0, countWait = 0, waitLoad;

                    PASOKA.forEach(images, function (image) {

                        if (image.hasOwnProperty("src")) {
                            imgObj = new Image();
                            imgObj.src = image.src;
                            imgObj.onload = function () {
                                countLoad++;
                            };
                        }
                    });

                    waitLoad = setInterval(function () {

                        if (countLoad >= images.length || countWait >= 3000) {
                            callback();
                            clearInterval(waitLoad);
                            return;
                        }

                        countWait++;
                    }, 50);
                },

                /**
                 * @private
                 * @description change slide by index
                 * @param {number} positionParam
                 */
                changeSlide = function changeSlide(positionParam) {

                    var slide;

                    if ("undefined" !== typeof slides) {

                        slide = slides.eq(positionParam);

                        if (slide.length > 0 && Number(slide.attr(dataImageAttr)) === positionParam) {

                            slides.css("display", "none");
                            slide.css("display", "block");
                            position = positionParam;
                        }
                    }
                },

                /**
                 * @private
                 * @returns {boolean}
                 */
                isResetable = function isResetable() {

                    if (!(slides instanceof $)) {
                        return false;
                    }
                    return position >= slides.length || position < 0;
                },

                /**
                 * @private
                 * @description loop slides
                 */
                loopSlides = function loopSlides() {

                    intervalSlide = setInterval(function () {

                        if (status === PASOKA.Slider.status.PLAY) {
                            if (isResetable()) {

                                if (!config.loop) {
                                    instance.pause();
                                    return;
                                }

                                position = 0;
                            }

                            changeSlide(position);
                            position++;
                        }

                    }, config.velocity);
                };

            config = configParam || config;


            // validate instance
            if ("undefined" === typeof instance) {
                return new PASOKA.Slider.Simple(configParam);
            }

            // validate jquery
            if ("undefined" === typeof $) {
                throw "PASOKA.Slider Error: jQuery not found";
            }

            // validate property container
            if (!config.hasOwnProperty("container")) {
                throw "PASOKA.Slider Error: property container not found";
            }

            // validate property velocity
            if (!PASOKA.typeValidate(config.velocity).isNumber()) {
                config.velocity = defaultVelocity;
            }

            // validate property loop
            if (!PASOKA.typeValidate(config.loop).isBoolean()) {
                config.loop = true;
            }

            // validate property autoPlay
            if (!PASOKA.typeValidate(config.autoPlay).isBoolean()) {
                config.autoPlay = true;
            }

            // validate property player
            if (config.player instanceof PASOKA.Slider.Player) {
                instance.addPlayer(config.player);
            }

            container = config.container || {};

            if (!(container instanceof $) && container.length <= 0) {
                throw "PASOKA.Slider Error: container not found ";
            }

            // pre load images
            preLoadImages(config.images, function () {

                var slide;

                container.empty();
                instance.stop();

                PASOKA.forEach(config.images, function (image) {

                    if (image.hasOwnProperty("src")) {

                        slide = $("<img>");
                        slide.attr("src", image.src);
                        slide.attr("class", "left small-12");
                        slide.attr(dataImageAttr, image.position);
                        slide.css("display", "none");
                        container.append(slide);
                    }
                });
                slides = container.find("img");

                if (config.autoPlay) {
                    instance.play();
                } else {
                    instance.first();
                }
            });


            /**
             * @public
             */
            this.play = function play() {

                if (status !== PASOKA.Slider.status.PLAY) {

                    status = PASOKA.Slider.status.PLAY;

                    if (isResetable()) {
                        position = 0;
                    }

                    loopSlides();
                    return instance;
                }
            };

            /**
             * @public
             */
            this.pause = function pause() {
                status = PASOKA.Slider.status.PAUSE;
                clearInterval(intervalSlide);
                return instance;
            };

            /**
             * @public
             */
            this.stop = function stop() {
                status = PASOKA.Slider.status.STOP;
                this.pause();
                changeSlide(0);
                return instance;
            };

            /**
             * @returns {Slider}
             */
            this.first = function first() {
                changeSlide(0);
                return instance;
            };

            /**
             * @returns {Slider}
             */
            this.last = function last() {
                changeSlide((slides.length - 1));
                return instance;
            };

            /**
             * @public
             */
            this.next = function next() {
                changeSlide(position + 1);
                return instance;
            };

            /**
             * @public
             */
            this.previous = function previous() {
                changeSlide(position - 1);
                return instance;
            };

            /**
             * @public
             */
            this.speedUp = function speedUp() {
                config.velocity -= 50;
                this.pause();
                this.play();
                return instance;
            };

            /**
             * @public
             */
            this.speedDown = function speedDown() {
                config.velocity += 100;
                this.pause();
                this.play();
                return instance;
            };

            /**
             * @public
             */
            this.loopOn = function loopOn() {
                config.loop = true;
                instance.play();
                return instance;
            };

            /**
             * @public
             */
            this.loopOff = function loopOff() {
                config.loop = false;
                instance.pause();
                return instance;
            };


            /**
             * @public
             * @description Create slider player
             * @param Player
             */
            this.addPlayer = function addPlayer(Player) {

                if (!(Player instanceof PASOKA.Slider.Player)) {
                    throw "PASOKA.Slider Error: Player invalid type";
                }

                config.player = Player;

                //first
                Player.click(Player.button.FIRST, function () {
                    instance.first();
                });

                //previous
                Player.click(Player.button.PREVIOUS, function () {
                    instance.previous();
                });

                //pause
                Player.click(Player.button.PAUSE, function () {
                    instance.pause();
                });

                //play
                Player.click(Player.button.PLAY, function () {
                    instance.play();
                });

                //next
                Player.click(Player.button.NEXT, function () {
                    instance.next();
                });

                //last
                Player.click(Player.button.LAST, function () {
                    instance.last();
                });

                //loop
                Player.click(Player.button.LOOP, function () {

                    var elm = $(this);

                    if (elm.attr("data-loop") === "on") {
                        elm.attr("data-loop", "off");
                        instance.loopOff();
                    } else {
                        elm.attr("data-loop", "on");
                        instance.loopOn();
                    }
                });

                //speed up
                Player.click(Player.button.SPEED_UP, function () {
                    instance.speedUp();
                });

                //speed down
                Player.click(Player.button.SPEED_DOWN, function () {
                    instance.speedDown();
                });
            };


            /**
             * @public
             * @description destroy slider
             */
            this.destroy = function destroy() {
                position = 0;
                config.velocity = defaultVelocity;
                container.empty();
                status = PASOKA.Slider.status.STOP;

                if (config.player instanceof PASOKA.Slider.Player) {
                    config.player.each(function (elm) {

                        if ("undefined" !== typeof elm && elm.hasOwnProperty("off")) {
                            elm.off("click");
                        }

                    });
                }

                clearInterval(intervalSlide);
            };
        };


    });

}(window, window.document, window.PASOKA = window.PASOKA || {}, window.jQuery));