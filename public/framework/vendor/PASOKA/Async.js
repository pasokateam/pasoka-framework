/*!
 * MODULE Async 2014-02-07
 *  ____
 * |    | PASOKA
 * |    | FRAMEWORK
 * |____| [JS]
 *
 * Dependencies:
 * - Core pasoka.js
 *
 * Author:
 * - Guilherme Santos <gsantos@climatempo.com.br, guilhermedossantos91@gmail.com>
 *
 *  MIT license, see the license in the project root
 */
(function (window, document, PASOKA) {

    "use strict";

    /**
     * @public
     * @namespace PASOKA.Async
     * @desc Modulo para realizar requisicoes assincronas
     * @version 1.0.0
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     */
    PASOKA.extend('Async', function Async() {

        /**
         * @public
         * @desc metodo autoexecutavel ao carregar modulo
         * @method main
         */
        this.main = function main() {};

        /**
         * @public
         * @desc carrega script dinamicamente pela url
         * @method loadScript
         * @param {string} url caminho do script que sera carregado
         * @param {function} callback script a ser executado apos load
         */
        this.script = function script(url, callback) {

            var js,
                head = document.getElementsByTagName('head');

            if ("undefined" !== typeof url && null !== head) {
                js = document.createElement("script");
                js.type = "text/javascript";
                js.src = url;
                if (null !== head[0]) {
                    head[0].appendChild(js);
                }
                if (js.readyState) {
                    js.onreadystatechange = function () {
                        if (js.readyState === "loaded" || js.readyState === "complete") {
                            js.onreadystatechange = null;
                            if ("function" === typeof callback) {
                                callback();
                            }
                        }
                    };
                } else {
                    js.onload = function () {
                        if ("function" === typeof callback) {
                            callback();
                        }
                    };
                }
            } else {
                throw  "Url invalid [>:|]";
            }
        };
    });
}(this, document, window.PASOKA = window.PASOKA || {}));