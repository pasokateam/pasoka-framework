/*!
 * MODULE Form 2014-02-07
 *  ____
 * |    | PASOKA
 * |    | FRAMEWORK
 * |____| [JS]
 *
 *
 * Dependencies:
 * - Core pasoka.js
 *
 * Author:
 * - Guilherme Santos <gsantos@climatempo.com.br, guilhermedossantos91@gmail.com>
 *
 *  MIT license, see the license in the project root
 */
(function (window, document, PASOKA) {

    "use strict";

    /**
     * @public
     * @namespace PASOKA.Form
     * @desc Modulo para tratar/validar informacoes de formularios
     * @version 1.0.0
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     */
    PASOKA.extend('Form', function Form() {

        /**
         * @public
         * @desc metodo autoexecutavel ao carregar modulo
         * @method main
         */
        this.main = function main() {};
    });
}(this, document, window.PASOKA = window.PASOKA || {}));