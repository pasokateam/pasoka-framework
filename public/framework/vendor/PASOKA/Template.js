/*!
 * MODULE Template 2014-02-08
 *  ____
 * |    | PASOKA
 * |    | FRAMEWORK
 * |____| [JS]
 *
 * Dependencies:
 * - Core pasoka.js
 *
 * Author:
 * - Guilherme Santos <gsantos@climatempo.com.br, guilhermedossantos91@gmail.com>
 *
 *  MIT license, see the license in the project root
 */
(function (window, document, PASOKA) {

    "use strict";

    /**
     * @public
     * @namespace PASOKA.Template
     * @desc
     * @version 1.0.0
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     */
    PASOKA.extend('Template', function Template() {

        /**
         * @public
         * @desc metodo autoexecutavel ao carregar modulo
         * @method main
         */
        this.main = function main() {

        };


        this.show = function show() {

            PASOKA.readTag('span', function(tag){
                var psk = tag.getAttribute('data-psk-var');
                if(!PASOKA.isNull(psk)) {

                    var name = tag.getAttribute('name');
                    tag.innerHTML = PASOKA.sandboxProperties[name];
                }

            });

            PASOKA.readTag('ul', function(tag){
                var psk = tag.getAttribute('data-psk-for');

                console.log(PASOKA.isNull(psk));
                console.log(PASOKA.isNull(psk));
                if(!PASOKA.isNull(psk)) {
                    var start = tag.getAttribute('data-start') || 0;
                    var end = tag.getAttribute('data-end');
                    var content = tag.innerHTML || "";
                    for(; start < (end-1); start++) {
                        content += content + "";
                        PASOKA.replaceAll(content, "<psk-index/>", start);
                    }
                    tag.innerHTML = content;
                }

            });
        };

    });
}(this, document, window.PASOKA = window.PASOKA || {}));