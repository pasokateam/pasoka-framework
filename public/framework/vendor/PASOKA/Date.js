/*!
 * MODULE Date 2014-02-07
 *  ____
 * |    | PASOKA
 * |    | FRAMEWORK
 * |____| [JS]
 *
 *
 * Dependencies:
 * - Core pasoka.js
 *
 * Author:
 * - Guilherme Santos <gsantos@climatempo.com.br, guilhermedossantos91@gmail.com>
 *
 *  MIT license, see the license in the project root
 */
(function (window, document, PASOKA) {

    "use strict";

    /**
     * @public
     * @namespace PASOKA.Date
     * @desc Modulo para validar, filtrar, calcular e/ou formatar datas
     * @version 1.0.0
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     */
    PASOKA.extend('Date', function Date() {

        /**
         * @public
         * @desc metodo autoexecutavel ao carregar modulo
         * @method main
         */
        this.main = function main() {};
    });
}(this, document, window.PASOKA = window.PASOKA || {}));