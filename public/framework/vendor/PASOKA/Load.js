/*!
 * MODULE Load 2015-01-011
 *  ____
 * |    | PASOKA
 * |    | FRAMEWORK
 * |____| [JS]
 *
 *
 * Dependencies:
 * - Core pasoka.js
 * - jQuery
 *
 * Author:
 * - Guilherme Santos <gsantos@climatempo.com.br, guilhermedossantos91@gmail.com>
 *
 *  MIT license, see the license in the project root
 */
(function (window, document, PASOKA, $) {

    "use strict";

    /**
     *
     */
    PASOKA.extend('Load', function Load() {

        /**
         *
         */
        this.show = function show() {
            $("#load").css("display", "block");
        };

        /**
         *
         */
        this.hide = function hide() {
            setTimeout(function () {
                $("#load").css("display", "none");
            }, 500);
        };
    });
}(window, document, window.PASOKA = window.PASOKA || {}, window.jQuery));