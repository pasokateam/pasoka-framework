/*!
 * MODULE Action 2014-02-08
 *  ____
 * |    | PASOKA
 * |    | FRAMEWORK
 * |____| [JS]
 *
 * Dependencies:
 * - Core pasoka.js
 *
 * Author:
 * - Guilherme Santos <gsantos@climatempo.com.br, guilhermedossantos91@gmail.com>
 *
 *  MIT license, see the license in the project root
 */
(function (window, document, PASOKA, $) {

    "use strict";

    /**
     * @public
     * @namespace PASOKA.Chart
     * @version 1.0.0
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     */
    PASOKA.extend('Chart', function Chart() {

        /**
         * @public
         * @method main
         */
        this.main = function main() {
        };

        /**
         * @param selectors
         * @constructor
         */
        this.Image = function Image(selectors) {

            var that = {},
                svgDataURL = function svgDataURL(svg) {
                    var svgAsXML = (new XMLSerializer()).serializeToString(svg);
                    return "data:image/svg+xml," + encodeURIComponent(svgAsXML);
                };

            that.canvas = undefined;
            that.context = undefined;
            that.a = document.createElement('a');
            that.a.style.display = "none";
            that.sourceImage = undefined;
            that.image = new window.Image();
            that.svg = undefined;

            //
            [].forEach.call(document.querySelectorAll(selectors), function (div) {

                that.svg = div.querySelector('svg');

                if ("undefined" !== typeof that.canvas) {
                    return false;
                } else if (that.svg) {

                    that.sourceImage = (that.svg ? svgDataURL(that.svg) : div.getAttribute('data-svgSource'));
                    that.image.src = that.sourceImage;
                    that.image.onload = function () {

                        that.canvas = document.createElement('canvas');
                        that.canvas.width = that.image.width;
                        that.canvas.height = that.image.height;

                        that.context = that.canvas.getContext('2d');
                        that.context.drawImage(that.image, 0, 0);

                        that.a.href = that.canvas.toDataURL('image/png');
                        document.body.appendChild(that.a);
                    };
                }
            });

            /**
             * @param {string} name
             */
            that.download = function download(name) {
                that.a.download = name;
                window.setTimeout(function () {
                    that.a.click();
                }, 700);

                return that;
            };

            /**
             * @param {string} name
             */
            that.persist = function persist(name) {
                window.setTimeout(function () {
                    $.ajax({
                        type: "POST",
                        url: "/highchart/persist",
                        data: {image: that.a.href, name: name}
                    });
                }, 700);

                return that;
            };

            return that;
        };


    });
}(window, document, window.PASOKA = window.PASOKA || {}, window.jQuery));