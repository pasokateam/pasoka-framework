/*!
 * MODULE Cookie 2014-02-07
 *  ____
 * |    | PASOKA
 * |    | FRAMEWORK
 * |____| [JS]
 *
 * Dependencies:
 * - Core pasoka.js
 *
 * Author:
 * - Guilherme Santos <gsantos@climatempo.com.br, guilhermedossantos91@gmail.com>
 *
 *  MIT license, see the license in the project root
 */
(function (window, document, PASOKA) {

    "use strict";

    /**
     * @public
     * @namespace PASOKA.Cookie
     * @desc Modulo para trabalhar com cookies
     * @version 1.0.0
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     */
    PASOKA.extend('Cookie', function Cookie() {

        var

            /**
             * @private
             * @desc configuracao dos cookies
             * @type {Object}
             */
                cnf = {

                /**
                 * @desc dominio do cookie
                 * @type {String}
                 */
                domain: location.host,

                /**
                 * @desc caminho do cookie
                 * @type {String}
                 */
                path: "/",

                /**
                 * @desc tempo de vida do cookie
                 * @type {Number}
                 */
                expires: 1,

                /**
                 * @desc seguranca
                 * @type {Boolean}
                 */
                secure: false
            };

        /**
         * @public
         * @desc metodo autoexecutavel ao carregar modulo
         * @method main
         */
        this.main = function main() {
            PASOKA.readTag('psk-cookie', function(tag){
                var name = tag.getAttribute('name'),
                    value = tag.getAttribute('value'),
                    expire = tag.getAttribute('expire');
                PASOKA.Cookie.set(name, value, expire);
            });
        };

        /**
         * @public
         * @method settings
         * @desc altera e retorna as configuracoes do Cookie
         * @param {Object} configParam
         * @return {Object}
         */
        this.settings = function settings(configParam) {
            if ("undefined" !== typeof configParam) {
                cnf.domain = configParam.domain || cnf.domain;
                cnf.path = configParam.path || cnf.path;
                cnf.expires = configParam.expires || cnf.expires;
                cnf.secure = configParam.secure || cnf.secure;
            }
            return cnf;
        };

        /**
         * @public
         * @method formatCookieString
         * @desc retorna a string para criar/modificar/remover cookies
         * @param {String} name
         * @param {String} value
         * @param {Date} expireDate
         * @return {String}
         */
        this.formatCookieString = function formatCookieString(name, value, expireDate) {
            var
                path = cnf.path ? '; path=' + cnf.path : '',
                domain = cnf.domain ? '; domain=' + cnf.domain : '',
                secure = cnf.secure ? '; secure' : '',
                expires = '; expires=' + expireDate.toGMTString(),
                encode = encodeURIComponent(value);
            return String(name + '=' + encode + expires + path + secure);
        };

        /**
         * @public
         * @method get
         * @desc obtem valor de um cookie
         * @param {String} cookieName
         * @return {String}
         */
        this.get = function get(cookieName) {
            var cStart, cEnd, cookie;
            if ("string" === typeof cookieName) {
                if (0 < document.cookie.length) {
                    cStart = document.cookie.indexOf(cookieName + '=');
                    if (cStart !== -1) {
                        cStart = cStart + cookieName.length + 1;
                        cEnd = document.cookie.indexOf(';', cStart);
                        cEnd = cEnd === -1 ? document.cookie.length : cEnd;
                        cookie = document.cookie;
                        cookie = cookie.substring(cStart, cEnd);
                        return decodeURIComponent(cookie);
                    }
                }
            }
            return null;
        };

        /**
         * @public
         * @method getString
         * @desc obtem valor de um cookie
         * @param {String} cookie
         * @return {String}
         */
        this.getString = function getString(cookie) {
            var cookieVal = this.get(cookie);
            if (null !== cookieVal) {
                cookieVal = String(cookieVal);
                return cookieVal.valueOf();
            }
            return null;
        };

        /**
         * @public
         * @method getNumber
         * @desc obtem valor de um cookie e o retorna como int
         * @param {String} cookie
         * @return {Number}
         */
        this.getNumber = function getNumber(cookie) {
            var cookieVal = this.get(cookie);
            if (null !== cookie) {
                return Number(cookieVal);
            }
            return null;
        };

        /**
         * @public
         * @method set
         * @desc cria/altera um cookie cookie
         * @param {String} name nome do cookie
         * @param {String} value valor do cookie
         * @param {Number} expire dias de vida
         */
        this.set = function set(name, value, expire) {
            var expireDate = new Date();
            expireDate.setDate(expireDate.getDate() + Number(expire || cnf.expires));
            document.cookie = this.formatCookieString(name, value, expireDate);
            return this;
        };

        /**
         * @public
         * @method remove
         * @desc remove um cookie
         * @param {String} name
         * @return {Boolean}
         */
        this.remove = function remove(name) {
            var expireDate = new Date();
            expireDate.setDate(expireDate.getDate() - 5);
            document.cookie = this.formatCookieString(name, "", expireDate);
            return !this.exists(name);
        };

        /**
         * @public
         * @method exists
         * @desc verifica se o cookie existe
         * @param {String} cookieName
         * @return {Boolean}
         */
        this.exists = function exists(cookieName) {
            return this.get(cookieName) ? true : false;
        };

        /**
         * @public
         * @constructor
         * @class List
         * @desc Classe para trabalhar com array dentro do cookie
         * @param {String} cookieName nome da lista
         * @param {Number} exp dias de vida
         * @return {Object}
         */
        this.List = function List(cookieName, exp) {

            var cookie, items;

            //Tempo de vida do cookie, 1 dia como padrao caso nao tenha sido definida
            exp = exp || cnf.expires;

            if ("undefined" === typeof cookieName) {
                throw "O parametro nome do Cookie.List nao foi atribuido!";
            }

            if (PASOKA.Cookie.get(cookieName) === null) {
                PASOKA.Cookie.set(cookieName, '', exp);
            }

            cookie = PASOKA.Cookie.get(cookieName);
            items = cookie ? cookie.split(/,/) : [];

            return {

                /**
                 * @public
                 * @method save
                 * @desc salva alteracoes
                 */
                save: function save() {
                    PASOKA.Cookie.set(cookieName, items.join(','), exp);
                    return this;
                },

                /**
                 * @public
                 * @method toArray
                 * @desc retorna valor do cookie em uma lista
                 * @return {Array}
                 */
                toArray: function toArray() {
                    return items;
                },

                /**
                 * @public
                 * @method toString
                 * @desc retorna todos os elementos em uma string
                 * @return {String}
                 */
                toString: function toString() {
                    return items.join(',');
                },

                /**
                 * @public
                 * @method isEmpty
                 * @desc retorna true se a lista estiver vazia
                 * @return {Boolean}
                 */
                isEmpty: function isEmpty() {
                    return items.length < 1 ? true : false;
                },

                /**
                 * @public
                 * @method size
                 * @desc retorna o total de items
                 * @return {Number}
                 */
                size: function size() {
                    return items.length;
                },

                /**
                 * @public
                 * @method indexOf
                 * @desc retorna posicao de um valor procurado ou -1 caso nenhum valor encontrado
                 * @param {String} elt
                 * @return {Number}
                 */
                indexOf: function indexOf(elt) {
                    var
                        total = items.length,
                        i = 0;

                    for (; i <= total; i++) {
                        if (elt === items[i]) {
                            return i;
                        }
                    }
                    return -1;
                },

                /**
                 * @public
                 * @method add
                 * @desc adicionar um novo item na lista
                 * @param {String} val
                 */
                add: function add(val) {
                    items.push(val);
                    this.save();
                    return this;
                },


                /**
                 * @public
                 * @method
                 * @desc obtem um elemento da lista
                 * @param {number} index
                 * @return {string}
                 */
                find: function find(index) {
                    var elm;
                    if ("undefined" !== typeof index) {
                        elm = items[index];
                        if ("undefined" !== typeof elm) {
                            return elm;
                        }
                    }
                    return null;
                },

                /**
                 * @public
                 * @method merge
                 * @desc adiciona um array no cookie, (limpando o atual)
                 * @param {Array} list
                 * @return {Boolean}
                 */
                merge: function merge(list) {
                    if ("undefined" !== typeof(list.length)) {
                        items = list;
                        this.save();
                        return true;
                    }
                    return false;
                },

                /**
                 * @public
                 * @method remove
                 * @desc remove um item da lista pelo valor
                 * @param {String} elt
                 * @return {Boolean}
                 */
                remove: function remove(elt) {
                    var pos = this.indexOf(elt);
                    if (pos !== -1) {
                        items.splice(pos, 1);
                        this.save();
                        return true;
                    }
                    return false;
                },

                /**
                 * @public
                 * @method removeByIndex
                 * @desc remove um item da lista pela posicao
                 * @param {Number} elt
                 * @return {Boolean}
                 */
                removeByIndex: function removeByIndex(elt) {
                    if ("undefined" !== typeof elt) {
                        items.splice(elt, 1);
                        this.save();
                        return true;
                    }
                    return false;
                },

                /**
                 * @public
                 * @method clear
                 * @desc limpar a lista
                 */
                clear: function clear() {
                    items.length = 0;
                    this.save();
                    return this;
                },

                /**
                 * @public
                 * @method exists
                 * @desc verifica se o item existe na lista
                 * @param {String} elt
                 * @return {Boolean}
                 */
                exists: function exists(elt) {
                    if (items.length > 0) {
                        if (this.indexOf(elt) !== -1) {
                            return true;
                        }
                    }
                    return false;
                },

                /**
                 * @public
                 * @method rename
                 * @desc renomear item pelo valor
                 * @param {String} search
                 * @param {String} newName
                 */
                rename: function rename(search, newName) {
                    var pos = this.indexOf(search);
                    if (pos !== -1) {
                        items[pos] = newName;
                        this.save();
                    }
                    return this;
                },

                /**
                 * @public
                 * @method renameByIndex
                 * @desc renomeia um item pela posicao
                 * @param {String} newName
                 * @param {Number} pos
                 */
                renameByIndex: function renameByIndex(newName, pos) {
                    if (typeof pos !== "undefined") {
                        if (typeof items[pos] !== "undefined") {
                            items[pos] = newName;
                            this.save();
                        }
                    }
                    return this;
                },


                /**
                 * @public
                 * @method update
                 * @desc atualiza valor de uma posicao
                 * @param {String} value
                 * @param {Number} index
                 */
                update: function update(index, value) {
                    this.renameByIndex(value, index);
                    return this;
                },

                /**
                 * @public
                 * @method search
                 * @desc retorna uma lista filtrada pelo valor da busca
                 * @param {String} val
                 * @return {Array}
                 */
                search: function search(val) {
                    var itemsFiltered = [],
                        i = 0,
                        total;

                    total = items.length;
                    for (; i <= total; i++) {
                        if (items[i] === val) {
                            itemsFiltered.push(i);
                        }
                    }
                    return itemsFiltered;
                }
            };
        };
    });
}(this, document, window.PASOKA));