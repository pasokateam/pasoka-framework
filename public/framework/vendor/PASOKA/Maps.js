/*!
 * MODULE Maps 2014-02-08
 *  ____
 * |    | PASOKA
 * |    | FRAMEWORK
 * |____| [JS]
 *
 * Dependencies:
 * - Core pasoka.js
 * - Google Maps V3 <https://developers.google.com/maps/documentation/javascript/?hl=pt-BR>
 *
 * Author:
 * - Guilherme Santos <gsantos@climatempo.com.br, guilhermedossantos91@gmail.com>
 *
 *  MIT license, see the license in the project root
 */
(function (window, document, PASOKA, undefined) {

    "use strict";

    /**
     * @public
     * @namespace PASOKA.Maps
     * @desc Modulo para trabalhar com mapas do google maps
     * @version 1.0.0
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     */
    PASOKA.extend('Google', function Google() {

        var

            /**
             * @private
             * @desc API do Google
             * @type {Object} window.google
             */
            google;

        /**
         * @public
         * @desc metodo autoexecutavel ao carregar modulo
         * @method main
         */
        this.main = function main() {

            // verifica se o google maps foi carregado
            if ("undefined" === typeof window.google) {
                throw "A Dependencia Google Maps nao foi encontrada [:(]";
            } else {
                google = window.google;
            }
        };

        /**
         * @constructor
         * @class Maps
         * @classdesc Classe para trabalhar com mapas
         * @param {string} container
         * @param {Object} options propriedades do mapa
         * @returns {Object}
         */
        this.Maps = function Maps(container, options) {

            var

                /**
                 * @private
                 * @desc Instancia do mapa
                 * @property
                 * @type {Object|null}
                 */
                mapInstance = null,

                /**
                 * @private
                 * @desc Container no mapa
                 * @type {Object|null}
                 */
                containerMap = null,

                /**
                 * @private
                 * @desc nome do container do mapa
                 * @type {string}
                 */
                containerName = container || "mapa",

                /**
                 * @private
                 * @desc Vetor de marcadores
                 * @property
                 * @type {Array}
                 */
                listMarkers = [],

                /**
                 * @private
                 * @desc Vetor de layers KML/KMZ
                 * @property
                 * @type {Array}
                 */
                listKmlLayers = [],

                /**
                 * @private
                 * @desc latitude e longitude
                 * @property
                 * @type {null}
                 */
                latlng = null,

                /**
                 * @private
                 * @desc zoom inicial do mapa
                 * @property
                 * @type {number}
                 */
                zoom = 13,

                /**
                 * @private
                 * @desc Vetor de layers GO
                 * @property
                 * @type {Array}
                 */
                listGOLayers = [];

            // verifica se o Maps foi iniciado como objeto
            if (!(this instanceof PASOKA.Google.Maps)) {
                return new PASOKA.Google.Maps(container, options);
            }

            // atribui configuracao padrao caso o parametro options nao tenha sido setado
            options = options || {
                zoom: 4,
                noClear: true,
                center: new google.maps.LatLng(-10.790141, -51.37207),
                disableDefaultUI: false,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE
                },
                streetViewControl: false,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.LEFT_TOP
                },
                mapTypeId: google.maps.MapTypeId.TERRAIN,
                scrollwheel: false
            };

            /**
             * @public
             * @desc seta opcoes do mapa
             * @method setOptions
             * @param {Object} opts
             */
            this.setOptions = function setOptions(opts) {

                // Verifica se o parametro é um objeto
                if ('object' === typeof opts) {
                    options = opts;
                }
            };


            /**
             * @public
             * @desc altera aparencia do mapa
             * @method setStyle
             * @param {string} type
             */
            this.setStyle = function setStyle(type) {
                var style;

                switch (type) {
                    case 'gray':
                        style = [
                            {
                                "featureType": "administrative", "elementType": "all", "stylers": [
                                {"visibility": "on"},
                                {"saturation": -100},
                                {"lightness": 20}
                            ]
                            },
                            {
                                "featureType": "road", "elementType": "all", "stylers": [
                                {"visibility": "on"},
                                {"saturation": -100},
                                {"lightness": 40}
                            ]
                            },
                            {
                                "featureType": "water", "elementType": "all", "stylers": [
                                {"visibility": "on"},
                                {"saturation": -10},
                                {"lightness": 30}
                            ]
                            },
                            {
                                "featureType": "landscape.man_made", "elementType": "all", "stylers": [
                                {"visibility": "simplified"},
                                {"saturation": -60},
                                {"lightness": 10}
                            ]
                            },
                            {
                                "featureType": "landscape.natural", "elementType": "all", "stylers": [
                                {"visibility": "simplified"},
                                {"saturation": -60},
                                {"lightness": 60}
                            ]
                            },
                            {
                                "featureType": "poi", "elementType": "all", "stylers": [
                                {"visibility": "off"},
                                {"saturation": -100},
                                {"lightness": 60}
                            ]
                            },
                            {
                                "featureType": "transit", "elementType": "all", "stylers": [
                                {"visibility": "off"},
                                {"saturation": -100},
                                {"lightness": 60}
                            ]
                            }
                        ];
                        break;
                    case 'dark':
                        style = [
                            {
                                "featureType": "all", "stylers": [
                                {"visibility": "off"}
                            ]
                            },
                            {
                                "featureType": "water", "stylers": [
                                {"visibility": "on"},
                                {"lightness": -100}
                            ]
                            }
                        ];
                        break;
                    case 'retro':
                        style = [
                            {
                                "featureType": "administrative", "stylers": [
                                {"visibility": "off"}
                            ]
                            },
                            {
                                "featureType": "poi", "stylers": [
                                {"visibility": "simplified"}
                            ]
                            },
                            {
                                "featureType": "road", "elementType": "labels", "stylers": [
                                {"visibility": "simplified"}
                            ]
                            },
                            {
                                "featureType": "water", "stylers": [
                                {"visibility": "simplified"}
                            ]
                            },
                            {
                                "featureType": "transit", "stylers": [
                                {"visibility": "simplified"}
                            ]
                            },
                            {
                                "featureType": "landscape", "stylers": [
                                {"visibility": "simplified"}
                            ]
                            },
                            {
                                "featureType": "road.highway", "stylers": [
                                {"visibility": "off"}
                            ]
                            },
                            {
                                "featureType": "road.local", "stylers": [
                                {"visibility": "on"}
                            ]
                            },
                            {
                                "featureType": "road.highway", "elementType": "geometry", "stylers": [
                                {"visibility": "on"}
                            ]
                            },
                            {
                                "featureType": "water", "stylers": [
                                {"color": "#84afa3"},
                                {"lightness": 52}
                            ]
                            },
                            {
                                "stylers": [
                                    {"saturation": -17},
                                    {"gamma": 0.36}
                                ]
                            },
                            {
                                "featureType": "transit.line", "elementType": "geometry", "stylers": [
                                {"color": "#3f518c"}
                            ]
                            }
                        ];
                        break;
                    case 'game':
                        style = [
                            {
                                "featureType": "water", "stylers": [
                                {"saturation": 43},
                                {"lightness": -11},
                                {"hue": "#0088ff"}
                            ]
                            },
                            {
                                "featureType": "road", "elementType": "geometry.fill", "stylers": [
                                {"hue": "#ff0000"},
                                {"saturation": -100},
                                {"lightness": 99}
                            ]
                            },
                            {
                                "featureType": "road", "elementType": "geometry.stroke", "stylers": [
                                {"color": "#808080"},
                                {"lightness": 54}
                            ]
                            },
                            {
                                "featureType": "landscape.man_made", "elementType": "geometry.fill", "stylers": [
                                {"color": "#ece2d9"}
                            ]
                            },
                            {
                                "featureType": "poi.park", "elementType": "geometry.fill", "stylers": [
                                {"color": "#ccdca1"}
                            ]
                            },
                            {
                                "featureType": "road", "elementType": "labels.text.fill", "stylers": [
                                {"color": "#767676"}
                            ]
                            },
                            {
                                "featureType": "road", "elementType": "labels.text.stroke", "stylers": [
                                {"color": "#ffffff"}
                            ]
                            },
                            {
                                "featureType": "poi", "stylers": [
                                {"visibility": "off"}
                            ]
                            },
                            {
                                "featureType": "landscape.natural", "elementType": "geometry.fill", "stylers": [
                                {"visibility": "on"},
                                {"color": "#b8cb93"}
                            ]
                            },
                            {
                                "featureType": "poi.park", "stylers": [
                                {"visibility": "on"}
                            ]
                            },
                            {
                                "featureType": "poi.sports_complex", "stylers": [
                                {"visibility": "on"}
                            ]
                            },
                            {
                                "featureType": "poi.medical", "stylers": [
                                {"visibility": "on"}
                            ]
                            },
                            {
                                "featureType": "poi.business", "stylers": [
                                {"visibility": "simplified"}
                            ]
                            }
                        ];
                        break;
                    case 'ir':
                        style = [{"stylers": [{"visibility": "on"}, {"saturation": -100}, {"gamma": 0.54}]}, {
                            "featureType": "road",
                            "elementType": "labels.icon",
                            "stylers": [{"visibility": "off"}]
                        }, {"featureType": "water", "stylers": [{"color": "#4d4946"}]}, {
                            "featureType": "poi",
                            "elementType": "labels.icon",
                            "stylers": [{"visibility": "off"}]
                        }, {
                            "featureType": "poi",
                            "elementType": "labels.text",
                            "stylers": [{"visibility": "simplified"}]
                        }, {
                            "featureType": "road",
                            "elementType": "geometry.fill",
                            "stylers": [{"color": "#ffffff"}]
                        }, {
                            "featureType": "road.local",
                            "elementType": "labels.text",
                            "stylers": [{"visibility": "simplified"}]
                        }, {
                            "featureType": "water",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#ffffff"}]
                        }, {
                            "featureType": "transit.line",
                            "elementType": "geometry",
                            "stylers": [{"gamma": 0.48}]
                        }, {
                            "featureType": "transit.station",
                            "elementType": "labels.icon",
                            "stylers": [{"visibility": "off"}]
                        }, {"featureType": "road", "elementType": "geometry.stroke", "stylers": [{"gamma": 7.18}]}];
                        break;
                    default:
                        break;
                }
                if ("undefined" !== typeof mapInstance) {
                    mapInstance.setOptions({styles: style});
                }

            };

            /**
             * @public
             * @desc inicia instancia do mapa
             * @method init
             */
            this.init = function init() {

                var element;

                if ("undefined" === typeof containerName) {
                    throw "O Container do mapa nao foi definido [=|] ";
                }

                element = document.getElementById(containerName);

                if (null === element) {
                    throw "O Container do mapa nao foi encontrado [=O] ";
                }

                // Verifica se o google maps ja nao foi iniciado
                if (null === mapInstance && 'object' === typeof options) {
                    containerMap = element;
                    mapInstance = new google.maps.Map(element, options);
                }
            };

            /**
             * @public
             * @desc retorna instancia do google maps
             * @method getInstance
             * @return {Object} google.maps.Map
             */
            this.getInstance = function getInstance() {
                return mapInstance;
            };

            /**
             * @public
             * @desc retorna container do mapa
             * @method getContainer
             * @returns {HTMLElement}
             */
            this.getContainer = function getContainer() {
                return document.getElementById(containerName);
            };

            /**
             * @public
             * @desc retorna nome do container do mapa
             * @method getContainerName
             * @returns {null|String}
             */
            this.getContainerName = function getContainerName() {
                return containerName;
            };

            /**
             * @public
             * @desc ajusta imagem do mapa caso o container sofra alguma alteracao no tamanho
             * @method resize
             */
            this.resize = function resize() {
                if (mapInstance instanceof google.maps.Map) {
                    google.maps.event.trigger(mapInstance, "resize");
                }
            };

            /**
             * @public
             * @desc força evento
             * @method trigger
             */
            this.trigger = function trigger(evt) {
                if (mapInstance instanceof google.maps.Map && "undefined" !== typeof evt) {
                    google.maps.event.trigger(mapInstance, evt);
                }
            };

            /**
             * @public
             * @desc reajusta tamanho do container
             * @method autoSize
             * @param {Number} height
             */
            this.autoSize = function autoSize(height) {

                height = "undefined" === typeof height ? 400 : height;
                height = Number(height) - 154;
                height = String(height) + "px";

                if (this.getContainer() !== null) {
                    this.getContainer().style.width = '100%';
                    this.getContainer().style.height = height;
                }

                if (mapInstance instanceof google.maps.Map) {
                    this.resize();
                }
            };


            /**
             * @public
             * @desc google maps set center
             * @param {window.google.maps.LatLng} latlng
             */
            this.setCenter = function setCenter(latlng) {
                if (mapInstance instanceof google.maps.Map) {
                    if (latlng instanceof window.google.maps.LatLng) {
                        mapInstance.setCenter(latlng);
                    }
                }
                return this;
            };

            /**
             * @public
             * @desc alias para atualizar o mapa
             * @method reload
             */
            this.reload = function reload() {
                this.resize();
            };


            /**
             * @public
             * @desc adiciona um novo botao no mapa
             * @param {object} params
             * @param {boolean} hide
             */
            this.createButton = function createButton(params, hide) {

                var controlDiv = window.document.createElement('div'),
                    controlUI = window.document.createElement('div'),
                    controlText = window.document.createElement('div'),
                    style,
                    position,
                    content;

                hide = hide || false;
                params = params || {};
                style = params.style || {};
                position = "string" === typeof params.position ? params.position.toUpperCase() : "LEFT_TOP";
                content = "string" === typeof params.content ? params.content : "";

                if (mapInstance instanceof google.maps.Map) {

                    if (hide) {
                        controlDiv.style.display = 'none';
                    }

                    controlDiv.style.padding = '5px';

                    //
                    controlUI.id = params.id || "";
                    controlUI.style.backgroundColor = style.backgroundColor || '#303030';
                    controlUI.style.borderStyle = style.borderStyle || 'solid';
                    controlUI.style.borderWidth = style.borderWidth || '2px';
                    controlUI.style.borderColor = style.borderColor || '#404040';
                    controlUI.style.cursor = style.cursor || 'pointer';
                    controlUI.style.textAlign = style.textAlign || 'center';
                    controlUI.style.padding = style.padding || '10px';
                    controlUI.style.marginLeft = style.marginLeft || '8px';
                    controlUI.style.marginTop = style.marginTop || '8px';
                    controlUI.title = style.title || '';
                    controlUI.style.align = "center";
                    controlDiv.appendChild(controlUI);

                    //
                    controlText.style.fontFamily = style.fontFamily || 'Arial,sans-serif';
                    controlText.style.fontSize = style.fontSize || '12px';
                    controlText.style.color = style.color || "#FFFFFF";
                    controlText.style.width = style.width || "32px";
                    controlText.style.textAlign = style.textAlign || 'center';
                    controlText.innerHTML = content || "";
                    controlText.title = style.title || '';
                    controlUI.appendChild(controlText);

                    if ("function" === typeof params.click) {
                        window.google.maps.event.clearListeners(controlUI, 'click');
                        window.google.maps.event.addDomListener(controlUI, 'click', function () {
                            params.click(controlUI);
                        });
                    }

                    mapInstance.controls[window.google.maps.ControlPosition[position] || window.google.maps.ControlPosition.LEFT_TOP].push(controlDiv);
                }
            };


            /**
             * @public
             * @const
             * @description controle de posicoes de elementos no mapa
             * @type {{BOTTOM_CENTER: *, BOTTOM_LEFT: (*|string), BOTTOM_RIGHT: (*|string), LEFT_BOTTOM: *, LEFT_CENTER: *, LEFT_TOP: *, RIGHT_BOTTOM: *, RIGHT_CENTER: *, RIGHT_TOP: *, TOP_CENTER: *, TOP_LEFT: (*|string), TOP_RIGHT: (*|string)}}
             */
            this.position = {
                "BOTTOM_CENTER": google.maps.ControlPosition.BOTTOM_CENTER,
                "BOTTOM_LEFT": google.maps.ControlPosition.BOTTOM_LEFT,
                "BOTTOM_RIGHT": google.maps.ControlPosition.BOTTOM_RIGHT,
                "LEFT_BOTTOM": google.maps.ControlPosition.LEFT_BOTTOM,
                "LEFT_CENTER": google.maps.ControlPosition.LEFT_CENTER,
                "LEFT_TOP": google.maps.ControlPosition.LEFT_TOP,
                "RIGHT_BOTTOM": google.maps.ControlPosition.RIGHT_BOTTOM,
                "RIGHT_CENTER": google.maps.ControlPosition.RIGHT_CENTER,
                "RIGHT_TOP": google.maps.ControlPosition.RIGHT_TOP,
                "TOP_CENTER": google.maps.ControlPosition.TOP_CENTER,
                "TOP_LEFT": google.maps.ControlPosition.TOP_LEFT,
                "TOP_RIGHT": google.maps.ControlPosition.TOP_RIGHT
            };

            /**
             * @public
             * @desc adiciona uma nova legenda no mapa
             * @param {object} params
             * @param {boolean} hide
             */
            this.createLegend = function createLegend(params) {

                var controlDiv = window.document.createElement('div'),
                    controlUI = window.document.createElement('div'),
                    controlText = window.document.createElement('div'),
                    style,
                    content;

                params = params || {};
                style = params.style || {};

                content = "string" === typeof params.content ? params.content : "";

                if (mapInstance instanceof google.maps.Map) {


                    controlDiv.style.padding = '5px';

                    //
                    controlUI.id = params.id || "";
                    controlUI.style.backgroundColor = style.backgroundColor || '#FFF';
                    controlUI.style.borderStyle = style.borderStyle || 'solid';
                    controlUI.style.borderWidth = style.borderWidth || '1px';
                    controlUI.style.borderColor = style.borderColor || '#a9a9a9';
                    controlUI.style.cursor = style.cursor || 'pointer';
                    controlUI.style.textAlign = style.textAlign || 'center';
                    controlUI.style.padding = style.padding || '6px';
                    controlUI.style.marginLeft = style.marginLeft || '8px';
                    controlUI.style.marginTop = style.marginTop || '8px';
                    controlUI.title = style.title || '';
                    controlUI.style.align = "center";
                    controlDiv.appendChild(controlUI);

                    //
                    controlText.style.fontFamily = style.fontFamily || 'Arial,sans-serif';
                    controlText.style.fontSize = style.fontSize || '12px';
                    controlText.style.color = style.color || "#303030";
                    //controlText.style.width = style.maxWidth || "202px";
                    controlText.style.textAlign = style.textAlign || 'left';
                    controlText.style.display = "table";
                    controlText.innerHTML = content || "";
                    controlText.title = style.title || '';
                    controlUI.appendChild(controlText);

                    if ("function" === typeof params.click) {
                        window.google.maps.event.clearListeners(controlUI, 'click');
                        window.google.maps.event.addDomListener(controlUI, 'click', function () {
                            params.click(controlUI);
                        });
                    }

                    mapInstance.controls[params.position || window.google.maps.ControlPosition.LEFT_TOP].push(controlDiv);
                }
            };


            /**
             * @public
             * @desc Função para estilizar o elemento do mapa, recebendo um objeto com parametros de CSS
             * @method addStyleCSS
             * @param {string} property
             * @param {string} value
             */
            this.addStyleCSS = function addStyleCSS(property, value) {

                var elm = this.getContainer();

                if (null !== elm) {
                    if ("undefined" !== typeof property) {

                        property = property.toLowerCase();
                        value = "undefined" === value ? "" : value.toLowerCase();
                        elm.style[property] = value;

                        if ("width" === property || "height" === property) {
                            this.resize();
                        }
                    }
                }
            };

            /**
             * @public
             * @desc muda z index dos marcadores
             * @method upMarkers
             */
            this.upMarkers = function upMarkers() {
                var
                    i = 0,
                    total = listMarkers.length,
                    marker;

                for (; i < total; i += 1) {
                    marker = listMarkers[i];
                    marker.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
                }
            };

            /**
             * @public
             * @desc adiciona marcador no mapa
             * @method addMarker
             * @param {object} optsMarker opções do marcador (google.maps.MarkerOptions)
             * @param {string} ref referência de busca/acesso do marcador
             * @param {Object} info janela de informação no marcador
             * @returns {Object|Null} {google.maps.Marker}
             */
            this.addMarker = function addMarker(optsMarker, ref, info) {

                var marker = null,
                    infowindow;

                // Verifica se o mapa foi instanciado e se as opções do marcadores foram especificadas (como objeto)
                if (mapInstance instanceof google.maps.Map && 'object' === typeof optsMarker) {

                    // Instancia marker no mapa
                    marker = new google.maps.Marker(optsMarker);

                    // Seta em qual mapa o marcador deve ficar (instancia atual)
                    marker.setMap(mapInstance);

                    marker.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);

                    // Verifica se a referência não foi definida, caso não, atribui um timestamp à ela
                    ref = "undefined" === typeof ref ? new Date().getTime() : ref;

                    // Verifica se foi passando alguma informação para ser exibida ao clicar  marcador
                    if ('object' === typeof info) {

                        // Instancia contêiner que exibirá a informação
                        infowindow = new google.maps.InfoWindow({
                            content: info.content,
                            maxWidth: info.width
                        });

                        // Criando listener para quando clicar no marcador exibir a janela de informação
                        google.maps.event.addListener(marker, 'click', function () {
                            infowindow.open(mapInstance, marker);
                        });
                    }

                    // Adiciona no final do vetor listMarkers
                    listMarkers.push({
                        'marker': marker,
                        'ref': ref
                    });
                }
                return marker;
            };

            /**
             * @public
             * @desc remove o marcador da lista e do mapa
             * @method removeMarkerByIndex
             * @param {Number} index
             * @return {Boolean}
             */
            this.removeMarkerByIndex = function removeMarkerByIndex(index) {

                var newListMarkers = [],
                    i = 0,
                    total = listMarkers.length;

                // Verifica se o index foi definido
                if ("undefined" !== typeof index) {

                    // Verifica se o elemento existe
                    if ("undefined" !== typeof listMarkers[index] && total > 0) {

                        // Remove marcador do mapa
                        listMarkers[index].marker.setMap(null);
                        listMarkers[index] = null;

                        // Percorre lista verifica os elementos que são diferentes de null e o coloca na nova lista
                        for (; i < total; i += 1) {
                            if (listMarkers[i] !== null && "undefined" !== typeof listMarkers[i]) {
                                newListMarkers.push(listMarkers[i]);
                            }
                        }

                        listMarkers = newListMarkers;
                        return true;
                    }
                }
                return false;
            };

            /**
             * @public
             * @desc remove marcadores pela referência
             * @method removeMarkersByRef
             * @param {string} value
             * @return {Number} total de elementos removidos
             */
            this.removeMarkersByRef = function removeMarkersByRef(value) {

                var countR = 0,
                    i,
                    total,
                    newListMarkers = [];

                if ("undefined" !== typeof value) {

                    total = listMarkers.length;
                    i = 0;

                    if (total > 0) {

                        // Percorre lista de marcadores e  verifica se o(s) elemento(s) existe(m) e o(s) remove
                        for (; i < total; i += 1) {
                            if ("undefined" !== typeof listMarkers[i]) {
                                if (listMarkers[i].ref === value) {
                                    listMarkers[i].marker.setMap(null);
                                    listMarkers[i] = null;
                                    delete listMarkers[i];
                                    countR++;
                                }
                            }
                        }

                        total = listMarkers.length;
                        i = 0;

                        // Percorre lista verifica os elementos que são diferentes de null e o coloca na nova lista
                        for (; i < total; i += 1) {
                            if (listMarkers[i] !== null && "undefined" !== typeof listMarkers[i]) {
                                newListMarkers.push(listMarkers[i]);
                            }
                        }
                        listMarkers = newListMarkers;
                    }
                }
                return countR;
            };

            /**
             * @public
             * @desc limpar marcadores
             * @method clearMarkers
             * @return {Number} total de elementos removidos
             */
            this.clearMarkers = function clearMarkers() {

                var countR = 0,
                    total = listMarkers.length,
                    i = 0;

                if (total > 0) {
                    for (; i < total; i += 1) {

                        // Remove marcador do mapa
                        listMarkers[i].marker.setMap(null);
                        listMarkers[i] = null;

                        countR++;
                    }
                    listMarkers.length = 0;
                }
                return countR;
            };

            /**
             * @public
             * @desc retorna todos os markers do maps
             * @method findAllMarkers
             * @returns {Array}
             */
            this.findAllMarkers = function findAllMarkers() {
                return listMarkers;
            };


            /**
             * @public
             * @desc Configucarao de clique no marker
             * @method markerClick
             * @param {boolean} opt
             */
            this.markerClick = function markerClick(opt) {

                var markers = this.findAllMarkers(),
                    total = markers.length,
                    i = 0,
                    marker;

                for (; i < total; i += 1) {
                    marker = markers[i].marker;
                    marker.setClickable(opt);
                }
            };

            /**
             * @public
             * @desc desabilita clique no marker
             * @method disableMarkerClick
             */
            this.disableMarkerClick = function disableMarkerClick() {
                this.markerClick(false);
            };

            /**
             * @public
             * @desc ativa clique no marker
             * @method enableMarkerClick
             */
            this.enableMarkerClick = function enableMarkerClick() {
                this.markerClick(true);
            };

            /**
             * @public
             * @desc adiciona uma nova camada KML/KMZ no mapa
             * @method addKmlLayer
             * @param {String} layer url da kml/kmz
             * @param {String} ref refêrencia de busca
             * @param {String|boolean} preserve
             * @returns {Object|null|undefined} google.maps.KmlLayer
             */
            this.addKmlLayer = function addKmlLayer(layer, ref, preserve) {

                var kml = null;

                preserve = preserve || true;

                // Verifica se o mapa foi instanciado e se a String layer foi atribuida
                if (mapInstance instanceof google.maps.Map && "undefined" !== typeof layer) {

                    // Instancia KmlLayer no mapa
                    kml = new google.maps.KmlLayer({
                        url: layer,
                        preserveViewport: preserve,
                        pane: "floatPane",
                        clickable: false
                    });

                    // Seta em qual mapa o KmlLayer deve ficar (instancia atual)
                    kml.setMap(mapInstance);

                    // Verifica se a referência não foi definida, caso não, atribui um timestamp à ela
                    ref = "undefined" === ref ? new Date().getTime() : ref;

                    // Adiciona no final do vetor listMarkers
                    listKmlLayers.push({
                        'kml': kml,
                        'ref': ref
                    });
                }
                return kml;
            };


            /**
             * @public
             * @desc adiciona uma nova camada KML/KMZ no mapa
             * @method addHideKmlLayer
             * @param {String} layer url da kml/kmz
             * @param {String} ref refêrencia de busca
             * @param {String} preserve
             * @returns {Object|null|undefined} google.maps.KmlLayer
             */
            this.addHideKmlLayer = function addHideKmlLayer(layer, ref, preserve) {

                var kml = null;

                preserve = preserve || true;

                // Verifica se o mapa foi instanciado e se a String layer foi atribuida
                if (mapInstance instanceof google.maps.Map && "undefined" !== typeof layer) {

                    // Instancia KmlLayer no mapa
                    kml = new google.maps.KmlLayer({
                        url: layer,
                        preserveViewport: preserve,
                        pane: "floatPane",
                        clickable: false
                    });

                    // Seta em qual mapa o KmlLayer deve ficar (instancia atual)
                    kml.setMap(null);

                    // Verifica se a referência não foi definida, caso não, atribui um timestamp à ela
                    ref = "undefined" === ref ? new Date().getTime() : ref;

                    // Adiciona no final do vetor listMarkers
                    listKmlLayers.push({
                        'kml': kml,
                        'ref': ref
                    });
                }
                return kml;
            };

            /**
             * @public
             * @desc remove a Layer pela posição no vetor
             * @method removeKmlLayerByIndex
             * @param {Number} index posição no vetor
             * @return {Boolean}
             */
            this.removeKmlLayerByIndex = function removeKmlLayerByIndex(index) {

                var i = 0,
                    total = listKmlLayers.length,
                    newListKmlLayers = [];

                // Verifica se o index foi definido
                if ("undefined" !== typeof index && total > 0) {

                    // Verifica se o elemento existe
                    if ("undefined" !== typeof listKmlLayers[index]) {

                        // Remove layer do mapa
                        listKmlLayers[index].kml.setMap(null);
                        listKmlLayers[index] = null;
                        delete listKmlLayers[index];

                        // Percorre lista verifica os elementos que são diferentes de null e o coloca na nova lista
                        for (; i < total; i += 1) {
                            if (listKmlLayers[i] !== null && "undefined" !== typeof listKmlLayers[i]) {
                                newListKmlLayers.push(listKmlLayers[i]);
                            }
                        }

                        //Atribui nova lista para a lista global
                        listKmlLayers = newListKmlLayers;
                        return true;
                    }
                }
                return false;
            };

            /**
             * @public
             * @desc obtem lista de kmls por referencia
             * @method findKmlByRef
             * @param {string} value
             * @type {Array}
             */
            this.findKmlLayerByRef = function findKmlLayerByRef(value) {

                var list = [],
                    i = 0,
                    total = listKmlLayers.length;

                for (; i < total; i += 1) {
                    if ("undefined" !== typeof listKmlLayers[i]) {
                        if (listKmlLayers[i].ref === value) {
                            list.push(listKmlLayers[i]);
                        }
                    }
                }

                return list;
            };


            /**
             * @public
             * @desc oculta ou mostra KML por referencia
             * @method toggleKmlLayerByRef
             * @param {string} value
             * @param {object} map
             */
            this.toggleKmlLayerByRef = function toggleKmlLayerByRef(value, map) {

                var i = 0,
                    total = listKmlLayers.length;

                map = map || null;

                for (; i < total; i += 1) {
                    if ("undefined" !== typeof listKmlLayers[i]) {
                        if (listKmlLayers[i].ref === value) {
                            listKmlLayers[i].kml.setMap(map);
                            return listKmlLayers[i].kml;
                        }
                    }
                }
                return null;
            };


            /**
             * @public
             * @desc oculta um KML por referencia
             * @method hideKmlLayerByRef
             * @param {string} value
             */
            this.hideKmlLayerByRef = function hideKmlLayerByRef(value) {
                this.toggleKmlLayerByRef(value, null);
            };

            /**
             * @public
             * @desc exibe um KML por referencia
             * @method showKmlLayerByRef
             * @param {string} value
             */
            this.showKmlLayerByRef = function showKmlLayerByRef(value) {
                return this.toggleKmlLayerByRef(value, mapInstance);
            };

            /**
             * @public
             * @desc remove kmls do mapa pela lista de Kmls
             * @method removeKmlLayerList
             * @param {Array} list Array de google.maps.KmlLayer
             * @type {Function}
             */
            this.removeKmlLayerList = function removeKmlLayerList(list) {

                var i,
                    total,
                    newList = [];

                if ("undefined" !== typeof list) {
                    if (Array.isArray(list)) {

                        i = 0;
                        total = list.length;
                        setTimeout(function () {

                            for (; i < total; i += 1) {
                                list[i].kml.setMap(null);
                            }

                            i = 0;
                            total = listKmlLayers.length;
                            for (; i < total; i += 1) {
                                if ("undefined" !== typeof listKmlLayers[i]) {
                                    if (listKmlLayers[i].kml.getMap() !== null) {
                                        newList.push(listKmlLayers[i]);
                                    }
                                }
                            }
                            listKmlLayers = newList;
                        }, 1000);
                    }
                }
            };

            /**
             * @public
             * @desc
             * @method findAllKml
             * @type {Function}
             * @return {Array}
             */
            this.findAllKml = function findAllKml() {
                return listKmlLayers;
            };

            /**
             * @public
             * @desc remove kmls da lista por referência
             * @method removeKmlLayerByRef
             * @param {String} value valor de referência
             * @return {Number} total de kmls removidas
             */
            this.removeKmlLayerByRef = function removeKmlLayerByRef(value) {

                var countR = 0,
                    newListKmlLayers = [],
                    i,
                    total;

                if ("undefined" !== typeof value) {

                    total = listKmlLayers.length;
                    i = 0;

                    if (total > 0) {

                        // Percorre lista de marcadores e  verifica se os elementos existem e os remove
                        for (; i < total; i += 1) {
                            if ("undefined" !== typeof listKmlLayers[i]) {
                                if (listKmlLayers[i].ref === value) {

                                    // Remove marcador do mapa
                                    listKmlLayers[i].kml.setMap(null);
                                    listKmlLayers[i] = null;
                                    delete listKmlLayers[i];
                                    countR++;
                                }
                            }
                        }

                        total = listKmlLayers.length;
                        i = 0;

                        // Percorre lista verifica os elementos que são diferentes de null e o coloca na nova lista
                        for (; i < total; i += 1) {
                            if (listKmlLayers[i] !== null) {
                                newListKmlLayers.push(listKmlLayers[i]);
                            }
                        }

                        //Atribui nova lista para a lista global
                        listKmlLayers = newListKmlLayers;
                    }
                }
                return countR;
            };

            /**
             * @public
             * @desc remove todas as layers KML/KMZ do mapa
             * @method clearKmlLayers
             * @return {Number} total de layers removidas
             */
            this.clearKmlLayers = function clearKmlLayers() {

                var countR = 0,
                    i = 0,
                    total = listKmlLayers.length;

                if (total > 0) {
                    for (; i < total; i += 1) {

                        // Remove layer do mapa
                        listKmlLayers[i].kml.setMap(null);
                        listKmlLayers[i] = null;
                        countR++;
                    }
                    listKmlLayers.length = 0;
                }
                return countR;
            };

            /**
             * @public
             * @desc adiciona um GroundOverLay no mapa
             * @method addGOLayer
             * @param {Object} layer {url:String, ref:mixed, bounds:google.maps.LatLngBounds, opacity:float}
             * @return {Object} google.maps.GroundOverlay
             */
            this.addGOLayer = function addGOLayer(layer) {

                var golayer = null;

                // Verifica se o parametro layer foi passado
                if (mapInstance instanceof google.maps.Map && "undefined" !== typeof layer) {

                    if ("undefined" !== typeof layer.url && layer.url !== null) {

                        // Instancia GroundOverLayer
                        if ("undefined" !== layer.bounds && layer.bounds !== null) {

                            // Instancia GroundOverLay
                            golayer = new google.maps.GroundOverlay(layer.url, layer.bounds);

                            // Seta mapa
                            golayer.setMap(mapInstance);

                            //Verifica se a opacidade é válida
                            layer.opacity = "undefined" === typeof layer.opacity || layer.opacity === null ? 1 : parseFloat(layer.opacity);

                            // Seta opacidade
                            golayer.setOpacity(layer.opacity);

                            // Verifica se a referência não foi definida, caso não, atribui um timestamp à ela
                            layer.ref = "undefined" === typeof layer.ref ? new Date().getTime() : layer.ref;

                            listGOLayers.push({
                                'golayer': golayer,
                                'ref': layer.ref
                            });
                        }
                    }
                }
                return golayer;
            };

            /**
             * @public
             * @desc remove a GroundOverLay pela posição no vetor
             * @method removeGOLayerByIndex
             * @param {Number} index posição no vetor
             * @returns {boolean}
             */
            this.removeGOLayerByIndex = function removeGOLayerByIndex(index) {

                var newListGOLayers = [],
                    i = 0,
                    total = listGOLayers.length;

                // Verifica se o index foi definido
                if ("undefined" !== typeof index && total > 0) {

                    // Verifica se o elemento existe
                    if (listGOLayers[index]) {

                        // Remove layer do mapa
                        listGOLayers[index].golayer.setMap(null);
                        listGOLayers[index] = null;
                        delete listGOLayers[index];

                        // Percorre lista verifica os elementos que são diferentes de null e o coloca na nova lista
                        for (; i < total; i += 1) {
                            if (listGOLayers[i] !== null && "undefined" !== typeof listGOLayers[i]) {
                                newListGOLayers.push(listGOLayers[i]);
                            }
                        }

                        //Atribui nova lista para a lista global
                        listGOLayers = newListGOLayers;
                        return true;
                    }
                }
                return false;
            };


            /**
             * @public
             * @desc obtem lista de GroundOverlay por referencia
             * @method findGOLayerByRef
             * @param {string} value
             * @returns {Array}
             */
            this.findGOLayerByRef = function findGOLayerByRef(value) {

                var list = [],
                    i = 0,
                    total = listGOLayers.length;

                for (; i < total; i += 1) {
                    if ("undefined" !== typeof listGOLayers[i]) {
                        if (listGOLayers[i].ref === value) {
                            list.push(listGOLayers[i]);
                        }
                    }
                }
                return list;
            };


            /**
             * @public
             * @desc retorna todas as GroundOverlayers adicionadas
             * @method findAllGOLayer
             * @returns {Array}
             */
            this.findAllGOLayer = function findAllGOLayer() {
                return listGOLayers;
            };

            /**
             * @public
             * @desc remove GroundOverlay da lista por referência
             * @method removeGOLayerByRef
             * @param {String} value valor de referência
             * @returns {Number} total de GroundOverlays removidos
             */
            this.removeGOLayerByRef = function removeGOLayerByRef(value) {

                var countR = 0,
                    newListGOLayers = [],
                    i = 0,
                    total = listGOLayers.length;

                if ("undefined" !== value && total > 0) {

                    // Percorre lista de marcadores e  verifica se os elementos existem e os remove
                    for (; i < total; i += 1) {
                        if ("undefined" !== typeof listGOLayers[i]) {
                            if (listGOLayers[i].ref === value) {
                                listGOLayers[i].golayer.setMap(null);
                                listGOLayers[i] = null;
                                delete listGOLayers[i];
                                countR++;
                            }
                        }
                    }

                    total = listGOLayers.length;
                    i = 0;

                    // Percorre lista verifica os elementos que são diferentes de null e o coloca na nova lista
                    for (; i < total; i += 1) {
                        if (listGOLayers[i] !== null && "undefined" !== typeof listGOLayers[i]) {
                            newListGOLayers.push(listGOLayers[i]);
                        }
                    }

                    //Atribui nova lista para a lista global
                    listGOLayers = newListGOLayers;
                }
                return countR;
            };

            /**
             * @public
             * @desc remove GroundOverLayers do mapa pela lista de Kmls
             * @method removeGOLayerList
             * @param {Array} list Array de google.maps.KmlLayer
             * @type {Function}
             */
            this.removeGOLayerList = function removeGOLayerList(list) {

                var i,
                    total,
                    newList = [];

                if ("undefined" !== typeof list) {
                    if (Array.isArray(list)) {

                        i = 0;
                        total = list.length;
                        setTimeout(function () {

                            for (; i < total; i += 1) {
                                listGOLayers[i].golayer.setMap(null);
                                //listGOLayers[i] = null;
                            }

                            i = 0;
                            total = listGOLayers.length;
                            for (; i < total; i += 1) {

                                if (listGOLayers[i] !== null && listGOLayers[i].golayer.getMap() !== null) {
                                    newList.push(listGOLayers[i]);
                                }
                            }
                            listKmlLayers = newList;
                        }, 500);
                    }
                }
            };

            /**
             * @public
             * @desc remove todas as layers GroundOverLay do mapa
             * @method clearGOLayers
             * @returns {Number} countR: total de GroundOverLay removida(s)
             */
            this.clearGOLayers = function clearGOLayers() {

                var countR = 0,
                    i = 0,
                    total = listGOLayers.length;

                if (total > 0) {
                    for (; i < total; i += 1) {
                        listGOLayers[i].golayer.setMap(null);
                        listGOLayers[i] = null;
                        countR++;
                    }
                    listGOLayers.length = 0;
                }
                return countR;
            };

            /**
             * @public
             * @desc Eventos do mapa
             * @link http://developers.google.com/maps/documentation/javascript/reference#event
             * @type {Object}
             */
            this.event = (function () {

                /**
                 * @private
                 * @type {Object}
                 */
                var that = {};

                /**
                 * @public
                 * @desc cria Listener
                 * @method addListener
                 * @param {String} eventName
                 * @param {function} handler
                 */
                that.addListener = function addListener(eventName, handler) {
                    if (mapInstance instanceof google.maps.Map) {
                        if ('string' === typeof eventName && 'function' === typeof handler) {
                            return google.maps.event.addListener(mapInstance, eventName, handler);
                        }
                    }
                    return null;
                };

                /**
                 * @public
                 * @desc cria ListenerOnce
                 * @method addListenerOnce
                 * @param {String} eventName
                 * @param {function} handler
                 */
                that.addListenerOnce = function addListenerOnce(eventName, handler) {
                    if (mapInstance instanceof google.maps.Map) {
                        if ('string' === typeof eventName && 'function' === typeof handler) {
                            return google.maps.event.addListenerOnce(mapInstance, eventName, handler);
                        }
                    }
                    return null;
                };

                /**
                 * @public
                 * @desc cria addDomListenerOnce
                 * @method addDomListenerOnce
                 * @param {String} eventName
                 * @param {function} handler
                 * @param {boolean} capture
                 */
                that.addDomListenerOnce = function addDomListenerOnce(eventName, handler, capture) {
                    if (mapInstance instanceof google.maps.Map) {
                        if ('string' === typeof eventName && 'function' === typeof handler) {
                            return google.maps.event.addDomListenerOnce(mapInstance, eventName, handler, ("undefined" !== typeof capture ? true : false));
                        }
                    }
                    return null;
                };

                /**
                 * @public
                 * @desc cria addDomListener
                 * @method addDomListener
                 * @param {String} eventName
                 * @param {function} handler
                 */
                that.addDomListener = function addDomListener(eventName, handler) {
                    if (mapInstance instanceof google.maps.Map) {
                        if ('string' === typeof eventName && 'function' === typeof handler) {
                            return google.maps.event.addDomListener(mapInstance, eventName, handler);
                        }
                    }
                    return null;
                };

                return that;
            })();
        };


    });


}(window, document, window.PASOKA = window.PASOKA || {}));