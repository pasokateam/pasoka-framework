/*!
 * MODULE Validation 2014-01-08
 *  ____
 * |    | PASOKA
 * |    | FRAMEWORK
 * |____| [JS]
 *
 * Dependencies:
 * - Core pasoka.js
 *
 * Author:
 * - Michael Dias <michael@climatempo.com.br>
 *
 *  MIT license, see the license in the project root
 */
(function (window, document, PASOKA) {

    "use strict";

    /**
     * @public
     * @namespace PASOKA.String
     * @desc Módulo para tratamento de strings
     * @version 1.0.0
     * @author Michael Dias <michael@climatempo.com.br>
     */
    PASOKA.extend('String', function Validation() {

        /**
         * @public
         * @desc método autoexecutavel ao carregar módulo
         * @method main
         */
        this.main = function main() {};

        /**
         * @public
         * @namespace PASOKA.String
         * @desc Transforma a primeira letra da string em maiúscula
         * @version 1.0.0
         * @author Michael Dias <michael@climatempo.com.br>
         */
        this.ucfirst = function(str) {
            str += '';

            var f = str.charAt(0).toUpperCase();

            return f + str.substr(1);
        };

    });
}(this, document, window.PASOKA = window.PASOKA || {}));