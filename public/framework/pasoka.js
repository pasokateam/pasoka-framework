/*!
 * CORE. 2014-02-07
 *  ____
 * |    | PASOKA
 * |    | FRAMEWORK
 * |____| [JS]
 *
 * Dependencies:
 * - RequireJS <http://www.requirejs.org>
 *
 * Author:
 * - Guilherme Santos <guilhermedossantos91@gmail.com>
 *
 *  MIT license, see the license in the project root
 */
(function (window, document, PASOKA) {

    "use strict";

    var
        require;

    if ("object" === typeof PASOKA.config) {
        PASOKA.loadController();
        return;
    }

    if ("undefined" === typeof window.require) {
        throw "PASOKA Error: Dependency RequireJS is not found!";
    } else {
        require = window.require;
    }

    require.config({
        //baseUrl: "/framework/",
        //enforceDefine: true,
        urlArgs: 'time=' + (new Date()).getTime(),
        //waitSeconds: 200,
        optimize: true,
        paths: {
            async: 'kernel/plugins/async',
            font: 'kernel/plugins/font',
            goog: 'kernel/plugins/goog',
            image: 'kernel/plugins/image',
            json: 'kernel/plugins/json',
            text: 'kernel/plugins/text',
            noext: 'kernel/plugins/noext',
            mdown: 'kernel/plugins/mdown',
            propertyParser: 'kernel/plugins/propertyParser'
        }
    });

    require(['json!config.json'], function (json) {

        if ("undefined" !== typeof json) {

            PASOKA = PASOKA || {};

            /**
             * @public
             * @type {string}
             */
            PASOKA.name = "PASOKA";

            /**
             * @public
             * @type {string}
             */
            PASOKA.version = "1.2.0";

            /**
             * @public
             * @description Project config JSON
             * @type {Object}
             */
            PASOKA.CONFIG = json || {
                    core: {},
                    project: {}
                };

            /**
             * @public
             * @description foreach
             * @param {Array} list
             * @param {function} callback
             */
            PASOKA.forEach = function forEach(list, callback) {

                var index = 0, total;

                if (PASOKA.typeValidate(callback).isFunction()) {

                    list = list || [];
                    total = list.length || 0;

                    for (; index < total; index += 1) {
                        if (callback(list[index], index)) {
                            break;
                        }
                    }
                }
            };

            /**
             * @public
             * @description check if Array is valid
             * @method isArray
             * @param {Array} list
             * @returns {boolean}
             */
            PASOKA.isArray = function isArray(list) {
                return Object.prototype.toString.call(list) === "[object Array]";
            };

            /**
             * @public
             * @description Find an item in array
             * @method inArray
             * @param {Array} list
             * @param {string} item
             * @returns {number}
             */
            PASOKA.inArray = function inArray(list, item) {

                var
                    i = 0,
                    total;

                if (PASOKA.isArray(list)) {
                    total = list.length;
                    for (; i < total; i += 1) {
                        if (list[i] === item) {
                            return i;
                        }
                    }
                }
                return -1;
            };


            /**
             * @public
             * @description shuffle array
             * @method shuffleArray
             * @param {Array} list
             * @returns {*}
             */
            PASOKA.shuffleArray = function shuffleArray(list) {

                var currentIndex,
                    temporaryValue,
                    randomIndex;

                if (PASOKA.isArray(list)) {
                    currentIndex = list.length;

                    while (0 !== currentIndex) {

                        randomIndex = Math.floor(Math.random() * currentIndex);
                        currentIndex -= 1;
                        temporaryValue = list[currentIndex];
                        list[currentIndex] = list[randomIndex];
                        list[randomIndex] = temporaryValue;
                    }
                }

                return list;
            };


            /**
             * @public
             * @description replace all values in string
             * @method replaceAll
             * @param {string} string
             * @param {string} search
             * @param {string} replace
             * @returns {string}
             */
            PASOKA.replaceAll = function replaceAll(string, search, replace) {
                return string.split(search).join(replace);
            };


            /**
             * @public
             * @description camel case string
             * @param {string} string
             * @returns {string|XML}
             */
            PASOKA.toCamelCase = function (string) {
                return string
                    .replace(/\s(.)/g, function ($1) {
                        return $1.toUpperCase();
                    })
                    .replace(/\s/g, '')
                    .replace(/^(.)/, function ($1) {
                        return $1.toLowerCase();
                    });
            };


            /**
             * @public
             * @description remove whitespaces in string
             * @param {string} value
             * @returns {string}
             */
            PASOKA.removeWhitespace = function removeWhitespace(value) {

                var
                    i = 0,
                    total,
                    newValue = "",
                    caracter;

                value = String(value) || "";
                total = value.length;
                for (; i < total; i += 1) {
                    caracter = value[i];
                    if (caracter !== " ") {
                        newValue += caracter;
                    }
                }
                return newValue;
            };


            /**
             * @public
             * @description remove accents in word
             * @method removeAccents
             * @param {string} value
             * @returns {string}
             */
            PASOKA.removeAccents = function removeAccents(value) {

                if (!PASOKA.typeValidate(value).isString()) {
                    return "";
                }

                value = value.replace(new RegExp('[\xc1\xc0\xc2\xc3\xc4\xe1\xe0\xe2\xe3\xe4]', 'gi'), 'a');
                value = value.replace(new RegExp('[\xc9\xc8\xca\xcb\xe9\xe8\xea\xeb]', 'gi'), 'e');
                value = value.replace(new RegExp('[\xcd\xcc\xce\xcf\xed\xec\xee\xef]', 'gi'), 'i');
                value = value.replace(new RegExp('[\xd3\xd2\xd4\xd5\xd6\xf3\xf2\xf4\xf5\xf6]', 'gi'), 'o');
                value = value.replace(new RegExp('[\xda\xd9\xdb\xdc\xfa\xf9\xfc]', 'gi'), 'u');
                value = value.replace(new RegExp('[\xe7]', 'gi'), 'c');

                return value;
            };

            /**
             * @public
             * @description check if value is null
             * @param {*} value
             * @returns {boolean}
             */
            PASOKA.isNull = function isNull(value) {
                return null === value ||
                    "null" === String(value).toLowerCase() ||
                    "undefined" === typeof value ||
                    "" === PASOKA.removeWhitespace(value);
            };


            /**
             * @public
             * @description module import list
             * @type {Array}
             */
            PASOKA.imports = [];

            /**
             * @public
             * @description include module JS (vendor)
             * @method include
             * @param {string} pkg
             */
            PASOKA.include = function include(pkg) {
                this.imports.push(pkg);
            };

            /**
             * @public
             * @description execute script framework sandbox
             * @method sandbox
             * @param {Array|Function} Arg1 dependency list or function
             * @param {Array|Function} Arg2 function
             */
            PASOKA.sandbox = function sandbox(Arg1, Arg2) {

                function loadArg1() {
                    return new Arg1(PASOKA.sandboxProperties);
                }

                function loadArg2() {
                    return new Arg2(PASOKA.sandboxProperties);
                }

                function getPath(list) {

                    var i = 0,
                        total,
                        item,
                        newList = [];

                    if (PASOKA.isArray(list)) {

                        total = list.length;

                        for (; i < total; i += 1) {

                            item = list[i];

                            if (item.indexOf('vendor') !== 0) {
                                item = 'vendor.' + item;
                            }

                            item = PASOKA.replaceAll(item, ".", "/");
                            newList.push(item);
                        }
                    }

                    return newList;
                }

                if (typeof Arg1 === "function") {

                    if (this.imports.length > 0) {

                        this.imports = getPath(this.imports);

                        require(this.imports, function () {
                            PASOKA.imports.length = 0;
                            PASOKA.ready(loadArg1);
                        });
                    } else {
                        PASOKA.ready(loadArg1);
                    }
                }

                if (typeof Arg2 === "function" && PASOKA.isArray(Arg2)) {

                    Arg1 = getPath(Arg1);

                    require(Arg1, function () {
                        PASOKA.ready(loadArg2);
                    });
                }
            };

            /**
             * @public
             * @method $
             * @description sandbox alias
             * @type {Function}
             */
            PASOKA.$ = function $(arg1, arg2) {
                return this.sandbox(arg1, arg2);
            };

            /**
             * @public
             * @description get data type
             * @param {boolean|string|number|function|null|Date|object} value
             * @returns {string}
             */
            PASOKA.typeOf = function typeOf(value) {
                var class2type = {
                    "[object Boolean]": PASOKA.type.BOOLEAN,
                    "[object Number]": PASOKA.type.NUMBER,
                    "[object String]": PASOKA.type.STRING,
                    "[object Function]": PASOKA.type.FUNCTION,
                    "[object Array]": PASOKA.type.ARRAY,
                    "[object Date]": PASOKA.type.DATE,
                    "[object RegExp]": PASOKA.type.REGEXP,
                    "[object Object]": PASOKA.type.OBJECT
                };
                return value === null ?
                    String(value) :
                class2type[Object.prototype.toString.call(value)] || "object";
            };


            /**
             * @public
             * @description type validate
             * @param {boolean|string|number|function|null|Date|object} value
             * @returns {{isBoolean: Function, isNumber: Function, isString: Function, isFunction: Function, isArray: Function, isDate: Function, isRegExp: Function, isObject: Function}}
             */
            PASOKA.typeValidate = function typeValidate(value) {
                return {
                    isBoolean: function isBoolean() {
                        return PASOKA.typeOf(value) === PASOKA.type.BOOLEAN;
                    },

                    isNumber: function isNumber() {
                        return PASOKA.typeOf(value) === PASOKA.type.NUMBER;
                    },

                    isString: function isString() {
                        return PASOKA.typeOf(value) === PASOKA.type.STRING;
                    },

                    isFunction: function isFunction() {
                        return PASOKA.typeOf(value) === PASOKA.type.FUNCTION;
                    },

                    isArray: function isArray() {
                        return PASOKA.typeOf(value) === PASOKA.type.ARRAY;
                    },

                    isDate: function isDate() {
                        return PASOKA.typeOf(value) === PASOKA.type.DATE;
                    },

                    isRegExp: function isRegExp() {
                        return PASOKA.typeOf(value) === PASOKA.type.REGEXP;
                    },

                    isObject: function isObject() {
                        return PASOKA.typeOf(value) === PASOKA.type.OBJECT;
                    }
                };
            };

            /**
             * @public
             * @description types
             * @type {{BOOLEAN: string, NUMBER: string, STRING: string, FUNCTION: string, ARRAY: string, DATE: string, REGEXP: string, OBJECT: string}}
             */
            PASOKA.type = {
                "BOOLEAN": "boolean",
                "NUMBER": "number",
                "STRING": "string",
                "FUNCTION": "function",
                "ARRAY": "array",
                "DATE": "date",
                "REGEXP": "regexp",
                "OBJECT": "object"
            };

            /**
             * @public
             * @description ready
             * @method ready
             */
            PASOKA.ready = (function () {

                var readyList,
                    DOMContentLoaded,
                    ReadyObj = {
                        isReady: false,
                        readyWait: 1,
                        holdReady: function holdReady(hold) {
                            if (hold) {
                                ReadyObj.readyWait++;
                            } else {
                                ReadyObj.ready(true);
                            }
                        },
                        ready: function ready() {

                            var wait = arguments[0] || "";

                            if ((wait === true && !--ReadyObj.readyWait) ||
                                (wait !== true && !ReadyObj.isReady)) {

                                if (!document.body) {
                                    return setTimeout(ReadyObj.ready, 1);
                                }

                                ReadyObj.isReady = true;
                                if (wait !== true && --ReadyObj.readyWait > 0) {
                                    return false;
                                }
                                readyList.resolveWith(document, [ReadyObj]);
                            }

                            return false;
                        },
                        bindReady: function bindReady() {

                            var topLevel;

                            if (readyList) {
                                return false;
                            }
                            readyList = ReadyObj._Deferred();

                            if (document.readyState === "complete") {
                                return setTimeout(ReadyObj.ready, 1);
                            }

                            // Mozilla, Opera e webkit
                            if (document.addEventListener) {
                                document.addEventListener("DOMContentLoaded", DOMContentLoaded, false);
                                window.addEventListener("load", ReadyObj.ready, false);

                                // IE
                            } else if (document.attachEvent) {

                                document.attachEvent("onreadystatechange", DOMContentLoaded);
                                window.attachEvent("onload", ReadyObj.ready);

                                topLevel = false;

                                try {
                                    topLevel = window.frameElement === null;
                                } catch (e) {
                                }

                                if (document.documentElement.doScroll && topLevel) {
                                    doScrollCheck();
                                }
                            }

                            return false;
                        },
                        _Deferred: function _Deferred() {

                            var
                                callbacks = [],
                                fired,
                                firing,
                                cancelled,
                                deferred = {

                                    done: function done() {

                                        var i,
                                            length,
                                            elem,
                                            type,
                                            _fired;

                                        if (!cancelled) {

                                            if (fired) {
                                                _fired = fired;
                                                fired = 0;
                                            }
                                            for (i = 0, length = arguments.length; i < length; i++) {
                                                elem = arguments[i];
                                                type = PASOKA.typeOf(elem);
                                                if (type === "array") {
                                                    deferred.done.apply(deferred, elem);
                                                } else if (type === "function") {
                                                    callbacks.push(elem);
                                                }
                                            }
                                            if (_fired) {
                                                deferred.resolveWith(_fired[0], _fired[1]);
                                            }
                                        }
                                        return this;
                                    },

                                    resolveWith: function resolveWith(context, args) {
                                        if (!cancelled && !fired && !firing) {
                                            args = args || [];
                                            firing = 1;
                                            try {
                                                while (callbacks[0]) {
                                                    callbacks.shift().apply(context, args);
                                                }
                                            }
                                            finally {
                                                fired = [context, args];
                                                firing = 0;
                                            }
                                        }
                                        return this;
                                    },

                                    resolve: function resolve() {
                                        deferred.resolveWith(this, arguments);
                                        return this;
                                    },

                                    /**
                                     isResolved: function isResolved() {
                                        return !!( firing || fired );
                                    },
                                     **/

                                    cancel: function cancel() {
                                        cancelled = 1;
                                        callbacks = [];
                                        return this;
                                    }
                                };

                            return deferred;
                        }
                    };

                function doScrollCheck() {
                    if (ReadyObj.isReady) {
                        return;
                    }

                    try {
                        document.documentElement.doScroll("left");
                    } catch (e) {
                        setTimeout(doScrollCheck, 1);
                        return;
                    }

                    ReadyObj.ready();
                }

                if (document.addEventListener) {
                    DOMContentLoaded = function () {
                        document.removeEventListener("DOMContentLoaded", DOMContentLoaded, false);
                        ReadyObj.ready();
                    };

                } else if (document.attachEvent) {
                    DOMContentLoaded = function () {
                        if (document.readyState === "complete") {
                            document.detachEvent("onreadystatechange", DOMContentLoaded);
                            ReadyObj.ready();
                        }
                    };
                }
                function ready(fn) {
                    ReadyObj.bindReady();
                    readyList.done(fn);
                }

                return ready;
            })();

            /**
             * @public
             * @description
             * @method trying
             * @param {string} dependency
             * @param {Function} callback
             * @type {Function}
             */
            PASOKA.trying = function trying(dependency, callback) {

                var
                    interval,
                    time = 100,
                    tryCounter = 500;

                if ("undefined" !== typeof dependency && "function" === typeof callback) {
                    interval = setInterval(function () {

                        if ("undefined" !== typeof window[dependency]) {
                            callback(window[dependency]);
                            clearInterval(interval);
                            return;
                        }
                        if (tryCounter <= 0) {
                            clearInterval(interval);
                            return;
                        }
                        tryCounter -= 1;
                    }, time);
                }
            };


            /**
             * @public
             * @param {object} obj1
             * @param {object} obj2
             * @returns {object}
             */
            PASOKA.mergeObjects = function mergeObjects(obj1, obj2) {
                var objFinal = {}, prop;
                for (prop in obj1) {
                    if (obj1.hasOwnProperty(prop)) {
                        objFinal[prop] = obj1[prop];
                    }
                }
                for (prop in obj2) {
                    if (obj2.hasOwnProperty(prop)) {
                        objFinal[prop] = obj2[prop];
                    }
                }
                return objFinal;
            };


            /**
             * @public
             * @description create new namespace in application
             * @method namespace
             * @param {string} nm
             * @param {function} Value
             * @returns {Object|null}
             */
            PASOKA.namespace = function namespace(nm, Value) {

                var
                    nsparts,
                    parent,
                    partname,
                    i,
                    total;

                if (typeof nm === "string") {

                    nsparts = nm.split(".");
                    parent = PASOKA;

                    if ("PASOKA" === nsparts[0]) {
                        nsparts = nsparts.slice(1);
                    }

                    total = nsparts.length;
                    for (i = 0; i < total; i += 1) {
                        partname = nsparts[i];
                        if ("undefined" === typeof parent[partname]) {
                            parent[partname] = {};
                        }
                        if (i === (total - 1)) {
                            if ("undefined" !== typeof Value) {
                                parent[partname] = new Value();
                            }
                        }
                        parent = parent[partname];
                    }

                    return parent;
                }
                return null;
            };


            /**
             * @public
             * @description create new module in framework
             * @method extend
             * @param {string} name
             * @param {Function|Object} Module
             */
            PASOKA.extend = function extend(name, Module) {

                var notModify = [
                        'extend',
                        'extends',
                        'sandbox',
                        '$',
                        'ready',
                        'isArray',
                        'inArray',
                        'imports',
                        'view',
                        'views',
                        'loadController',
                        'setParam',
                        'CONFIG',
                        'namespace',
                        'include'
                    ],
                    obj;

                if ("string" === typeof name &&
                    ("function" === typeof Module || "object" === typeof Module)) {

                    name = name.replace('PASOKA.', '');

                    if (PASOKA.inArray(notModify, name) !== -1) {
                        throw "PASOKA Error: Module " + name + " already exists";
                    }

                    obj = PASOKA.namespace(name, Module);

                    if (null !== obj) {
                        if ("function" === typeof obj.main) {
                            obj.main();
                        }
                    } else {
                        throw "PASOKA Error: Could not create module ";
                    }


                } else {
                    throw "PASOKA Error: invalid module name";
                }
            };

            /**
             * @public
             * @description views loaded list
             * @type {Array}
             */
            PASOKA.views = [];

            /**
             * @public
             * @description params view list
             * @type {Array}
             */
            PASOKA.params = [];


            /**
             * @public
             * @description properties sandbox
             * @type {{}}
             */
            PASOKA.sandboxProperties = {};

            /**
             * @public
             * @description add new property in sandbox
             * @method properties
             * @param {object} paramProperties
             */
            PASOKA.properties = function properties(paramProperties) {
                if ("object" === typeof paramProperties && !PASOKA.isNull(paramProperties)) {
                    PASOKA.sandboxProperties = paramProperties;
                }
            };

            /**
             * @public
             * @description get view param
             * @method getParam
             * @param {String} param
             * @returns {string|null|array|number|boolean|number[]|string[]}
             */
            PASOKA.getParam = function getParam(param) {
                if ("undefined" !== typeof PASOKA.params[param]) {
                    return PASOKA.params[param];
                }
                return null;
            };

            /**
             * @public
             * @description add new view param
             * @method setParam
             * @param {string} param
             * @param {string} value
             * @param {string} type
             * @type {Function}
             */
            PASOKA.setParam = function setParam(param, value, type) {

                type = type || "";

                if ("undefined" !== typeof param && "undefined" !== typeof value) {
                    if (null !== param && null !== value) {
                        switch (type) {
                            case "string":
                            case "str":
                            case "char":
                            case "varchar":
                                PASOKA.params[param] = value;
                                break;
                            case "int":
                            case "float":
                            case "double":
                            case "numeric":
                            case "decimal":
                            case "number":
                                PASOKA.params[param] = parseInt(value);
                                break;
                            case "bool":
                            case "boolean":
                                PASOKA.params[param] = Boolean(value);
                                break;
                            case "list":
                            case "array":
                                PASOKA.params[param] = value.split(",");
                                break;
                            case "array-number":
                                PASOKA.params[param] = [];
                                PASOKA.forEach(value.split(","), function (number) {
                                    PASOKA.params[param].push(parseFloat(number));
                                });
                                break;
                            case "array-string":
                                PASOKA.params[param] = [];
                                PASOKA.forEach(value.split(","), function (number) {
                                    PASOKA.params[param].push(String(number).valueOf());
                                });
                                break;
                            default:
                                PASOKA.params[param] = String(value);
                                break;
                        }
                    }
                }
            };

            /**
             * @public
             * @description get core config
             * @method getConfigCore
             * @param {string} config
             * @returns {*}
             */
            PASOKA.getConfigCore = function getConfigCore(config) {
                var core = PASOKA.CONFIG.core || {};
                if (core.hasOwnProperty(config)) {
                    return core[config];
                }
                throw "PASOKA Error: Core config " + config + " not found";

            };

            /**
             * @public
             * @description get project config
             * @method getConfigProject
             * @param {string} config
             * @returns {*}
             */
            PASOKA.getConfigProject = function getConfigProject(config) {
                var project = PASOKA.CONFIG.project || {};
                if (project.hasOwnProperty(config)) {
                    return project[config];
                }
                throw "PASOKA Error: Project config" + config + " not found";
            };

            /**
             * @public
             * @description load controller
             * @method view
             * @param {string} controllerName
             * @param {Array} params
             * @type {Function}
             */
            PASOKA.view = function view(controllerName, params) {

                var
                    i = 0,
                    total,
                    param,
                    path = PASOKA.getConfigCore('path') || {
                            controller: ""
                        };

                if ("undefined" !== typeof controllerName && controllerName !== "") {

                    require.undef(path.controller + controllerName);

                    if (PASOKA.isArray(params)) {
                        total = params.length;
                        for (; i < total; i += 1) {
                            param = params[i];
                            if ("object" === typeof param) {
                                PASOKA.setParam(param.name, param.value, param.type);
                            }
                        }
                    }
                    if (PASOKA.inArray(PASOKA.views, controllerName) === -1) {
                        require([path.controller + controllerName]);
                        PASOKA.views.push(controllerName);
                    } else {
                        require.undef(path.controller + controllerName);
                        require([path.controller + controllerName]);
                    }
                }
            };

            /**
             * @public
             * @description read new tags
             * @method readTag
             * @param {string} tagName
             * @param {Function} callback
             */
            PASOKA.readTag = function readTag(tagName, callback) {

                var tags,
                    i = 0,
                    total;

                if ("string" === typeof tagName && "function" === typeof callback) {
                    tags = document.getElementsByTagName(tagName);
                    if ("undefined" !== typeof tags) {
                        total = tags.length;

                        for (; i < total; i++) {
                            callback(tags[i], total);
                        }
                    }
                }
            };

            /**
             * @public
             * @description load controller template
             * @method loadController
             */
            PASOKA.loadController = function loadController() {

                var
                    params,
                    view,
                    controller,
                    i = 0,
                    total,
                    listParam = [];

                PASOKA.ready(function () {

                    /*!
                     * PASOKA JS PARAMS
                     */
                    if (typeof window.pskParams !== "undefined") {
                        for (var param in window.pskParams) {
                            if (window.pskParams.hasOwnProperty(param)) {
                                PASOKA.params[param] = window.pskParams[param];
                            }
                        }
                        delete window.pskParams;
                    }


                    /*!
                     * GET VIEW BY TAG PSK
                     */
                    view = document.getElementsByTagName('psk-view');

                    if ("undefined" !== typeof view) {
                        if ("undefined" !== typeof view[0]) {

                            view = view[0];
                            controller = view.getAttribute('controller');
                            params = document.getElementsByTagName('psk-param');
                            i = 0;
                            total = params.length;

                            for (; i < total; i += 1) {
                                listParam.push({
                                    'name': params[i].getAttribute('name'),
                                    'value': params[i].getAttribute('value'),
                                    'type': params[i].getAttribute('type')
                                });
                            }
                        }
                    }

                    /*!
                     * GET VIEW BY TAG HTML
                     */
                    view = document.getElementsByTagName('p');

                    if ("undefined" !== typeof view && view.length > 0) {

                        view = view[0];
                        if (null !== view.getAttribute('data-view')) {

                            view.style.display = 'none';
                            controller = view.getAttribute('data-controller');
                            params = view.getElementsByTagName('i');
                            total = params.length;
                            i = 0;

                            for (; i < total; i += 1) {
                                if (null !== params[i].getAttribute('data-param')) {
                                    listParam.push({
                                        'name': params[i].getAttribute('data-name'),
                                        'value': params[i].getAttribute('data-value'),
                                        'type': params[i].getAttribute('data-type')
                                    });
                                }
                            }

                        }
                    }
                    PASOKA.view(controller, listParam);
                });
            };
            PASOKA.loadController();

        } else {
            throw "PASOKA Error: File config.json not found";
        }
    });

    // alias
    //if (!window.PSK) {
    //    window.PSK = window.PASOKA;
    //}
}(window, document, window.PASOKA = window.PASOKA || {}));