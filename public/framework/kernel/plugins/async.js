(function (window) {

    "use strict";

    window.define(function () {

        var

            /**
             * @private
             * @constant
             * @type {string}
             */
            DEFAULT_PARAM_NAME = 'callback',

            /**
             * @private
             * @type {number}
             */
            _uid = 0;

        /**
         * @private
         * @param {string} src
         */
        function injectScript(src) {
            var s, t;
            s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = src;
            t = document.getElementsByTagName('script')[0];
            t.parentNode.insertBefore(s, t);
        }

        /**
         * @private
         * @param {string} name
         * @param {string} id
         * @returns {string}
         */
        function formatUrl(name, id) {
            var paramRegex = /!(.+)/,
                url = name.replace(paramRegex, ''),
                param = (paramRegex.test(name)) ? name.replace(/.+!/, '') : DEFAULT_PARAM_NAME;
            url += (url.indexOf('?') < 0) ? '?' : '&';
            return url + param + '=' + id;
        }

        /**
         * @private
         * @returns {string}
         */
        function uid() {
            _uid += 1;
            return '__async_req_' + _uid + '__';
        }

        return {
            load: function (name, req, onLoad, config) {
                if (config.isBuild) {
                    onLoad(null);
                } else {
                    var id = uid();
                    window[id] = onLoad;
                    injectScript(formatUrl(name, id));
                }
            }
        };
    });
}(window));