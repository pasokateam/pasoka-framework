<?php
/*!
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
require_once '../app/bootstrap.php';

/**
 * DO NOT MODIFY.
 *
 * @version 1.0.0
 */
$router = Pasoka\Component\Route\Router::getInstance();
$class = Pasoka\Component\Http\Request\Get::getString('class');
$method = Pasoka\Component\Http\Request\Get::getString('method');
$loader = new Pasoka\Core\Dependence\PasokaLoader(new Bootstrap());

if (!$router->run($class, $method)) {

    // log
    $loader
        ->getLogger()
        ->warning("Controller not working, CLASS:{$class}, METHOD:{$method}, URL:{$_SERVER["REQUEST_URI"]}");

    // page 404
    $loader
        ->getTemplate()
        ->setCache(false, -1)
        ->setVar('url', $_SERVER["REQUEST_URI"])
        ->show('error404.tpl');

};